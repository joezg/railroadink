<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * railroadink.action.php
 *
 * railroadink main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/railroadink/railroadink/myAction.html", ...)
 *
 */


class action_railroadink extends APP_GameAction
{
  // Constructor: please do not modify
  public function __default()
  {
    if (self::isArg('notifwindow')) {
      $this->view = "common_notifwindow";
      $this->viewArgs['table'] = self::getArg("table", AT_posint, true);
    } else {
      $this->view = "railroadink_railroadink";
      self::trace("Complete reinitialization of board game");
    }
  }

  public function drawRoute()
  {
    self::setAjaxMode();

    // Retrieve arguments
    // Note: these arguments correspond to what has been sent through the javascript "ajaxcall" method
    $route = self::getArg("route", AT_posint, true);
    $routeType = self::getArg("routeType", AT_posint, true);
    $rotation = self::getArg("rotation", AT_int, true);
    $flipped = self::getArg("flipped", AT_posint, true);
    $x = self::getArg("x", AT_posint, true);
    $y = self::getArg("y", AT_posint, true);
    $dieId = self::getArg("dieId", AT_posint, false);
    $specialId = self::getArg("specialId", AT_posint, false);

    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->drawRoute($route, $routeType, $rotation, $flipped, $x, $y, $dieId, $specialId);
    self::ajaxResponse();
  }
  
  public function doneUseDice()
  {
    self::setAjaxMode();

    // Retrieve arguments
    // no arguments

    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->doneUseDice();
    self::ajaxResponse();
  }
  
  public function undoUseDice()
  {
    self::setAjaxMode();

    // Retrieve arguments
    // no arguments

    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->undoUseDice();
    self::ajaxResponse();
  }
  
  public function restartUseDice()
  {
    self::setAjaxMode();

    // Retrieve arguments
    // no arguments

    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->restartUseDice();
    self::ajaxResponse();
  }
  
  public function decideMeteor()
  {
    self::setAjaxMode();

    // Retrieve arguments
    $x = self::getArg("x", AT_posint, true);
    $y = self::getArg("y", AT_posint, true);

    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->decideMeteor($x, $y);
    self::ajaxResponse();
  }
  
  public function decidePrevent()
  {
    self::setAjaxMode();

    // Retrieve arguments
    $route = self::getArg("route", AT_posint, true);
    
    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->decidePrevent($route);
    self::ajaxResponse();
  }
  
  public function allowMeteor()
  {
    self::setAjaxMode();

    // Retrieve arguments
    //no arguments
    // // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
    $this->game->allowMeteor();
    self::ajaxResponse();
  }
}
