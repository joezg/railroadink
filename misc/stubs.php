<?php 

class Table {
    public function __construct() {
    }

    public static function getNew(string $service) : Deck { return new Deck(); }

    /**
     * This method should be located at the beginning of yourgamename.php. This is where you define the globals used in your game logic, by assigning them IDs. 
     */
    public function initGameStateLabels(array $gameStateDefinition) : void {}
    /** 
     * Initialize your global value. Must be called before any use of your global, so you should call this method from your "setupNewGame" method.
     */
    public function setGameStateInitialValue( string $value_label, $value_value ) {}
    /** 
     * Retrieve the current value of a global.
     */
    public function getGameStateValue( $value_label ) : int { return 0;}
    /**
     * Set the current value of a global.
     */
    public function setGameStateValue( $value_label, $value_value ) {}
    /**
     * Increment the current value of a global. If increment is negative, decrement the value of the global.
     * Return the final value of the global. 
     */
    public function incGameStateValue( $value_label, $increment ) {}
    
    public function getGameinfos(): array { return []; }
    public function reattributeColorsBasedOnPreferences() : void {}
    public function reloadPlayersBasicInfos(): void {}
    public function loadPlayersBasicInfos(): array { return []; }
    
    public function DbQuery(string $sql) : void {}
    public function DbAffectedRow() : int { return 0;}
    public function getCollectionFromDb(string $sql) : array { return []; }
    public function getObjectListFromDB(string $sql) : array { return []; }
    public function getObjectFromDB(string $sql) : array { return []; }
    public function getUniqueValueFromDB(string $sql) { return 0; }
    public function applyDbUpgradeToAllDB(string $sql) { return 0; }
    
    public function activeNextPlayer():void {}
    public function getActivePlayerId() : int { return 0;}
    public function getPlayerAfter(int $playerId) : int { return 0;}
    public function getCurrentPlayerId() : int { return 0; }
    public function getCurrentPlayerName() : string { return ""; }
    public function getActivePlayerName() : string { return ""; }

    public function notifyAllPlayers( $notification_type, $notification_log, $notification_args ) {}
    public function notifyPlayer( $player_id, $notification_type, $notification_log, $notification_args ) {}

    public static function _(string $message) : string { return ""; }

    public function initStat(string $type, string $name, $value, int $playerId = null) : void {}
    public function setStat(string $value, string $name, int $playerId = null) : void {}
    public function incStat(int $delta, string $name, int $playerId = null) : void {}
    public function getStat(string $name, int $playerId = null) : int { return 0;}

    /** @var StateMachine */
    protected $gamestate;
    /**
     * Check if an action is valid for the current game state, and optionally, throw an exception if it isn't.
     * The action is valid if it is listed in the "possibleactions" array for the current game state (see game state description).
     * This method MUST be the first one called in ALL your PHP methods that handle player actions, in order to make sure a player doesn't perform an action not allowed by the rules at the point in the game.
     * If "bThrowException" is set to "false", the function returns false in case of failure instead of throwing an exception. This is useful when several actions are possible, in order to test each of them without throwing exceptions.
     */
    public function checkAction( $actionName, $bThrowException=true ) {}

    public function giveExtraTime($playerId, $specificTime = null) : void {}
    
}

class BgaUserException {
    public function __construct(string $message) {
    }
}

class StateMachine {
    /**
     * Change current state to a new state. Important: the $transition parameter is the name of the transition, and NOT the name of the target game state, see Your game state machine: states.inc.php for more information about states.
     */
    public function nextState( $transition = "" ) {}
    
    /**
     * (rarely used)
     * This works exactly like "checkAction" (above), except that it does NOT check if the current player is active.
     * This is used specifically in certain game states when you want to authorize additional actions for players that are not active at the moment.
     * Example: in Libertalia, you want to authorize players to change their mind about the card played. They are of course not active at the time they change their mind, so you cannot use "checkAction"; use "checkPossibleAction" instead.
     */
    public function checkPossibleAction( $action ) {}
    
    /**
     * Get an associative array of current game state attributes, see Your game state machine: states.inc.php for state attributes.
     */
    public function state() {}
    public function state_id() : int { return 0;}

    public function setPlayerNonMultiactive() {}
    public function setAllPlayersMultiactive() {}
    public function setPlayersMultiactive($players, $next_state, $bExclusive = false ) : bool { return false;}
    public function updateMultiactiveOrNextState( $nextState ) {}
    public function getActivePlayerList() : array { return []; }
    public function changeActivePlayer(int $playerId) { }
}

class Deck {
    public function init( $table_name ) {}
    public function createCards( $cards, $location='deck', $location_arg=null ) {}
    public function pickCard( $location, $player_id ) {}
    public function pickCards( $nbr, $location, $player_id ) {}
    public function pickCardForLocation( $from_location, $to_location, $location_arg=0 ) {}
    public function pickCardsForLocation( $nbr, $from_location, $to_location, $location_arg=0, $no_deck_reform=false ) {}
    public function moveCard( $card_id, $location, $location_arg=0 ) {}
    public function moveCards( $cards, $location, $location_arg=0 ) {}
    public function insertCard( $card_id, $location, $location_arg ) {}
    public function insertCardOnExtremePosition( $card_id, $location, $bOnTop ) {}
    public function moveAllCardsInLocation( $from_location, $to_location, $from_location_arg=null, $to_location_arg=0 ) {}
    public function moveAllCardsInLocationKeepOrder( $from_location, $to_location ) {}
    public function playCard( $card_id ) {}
    public function getCard( $card_id ) {}
    public function getCards( $cards_array ) {}
    public function getCardsInLocation( $location, $location_arg = null, $order_by = null ) : array { return []; }
    public function countCardInLocation( $location, $location_arg=null ) {}
    public function countCardsInLocations($location, $location_arg=null) {}
    public function countCardsByLocationArgs( $location ) {}
    public function getPlayerHand( $player_id ) {}
    public function getCardOnTop( $location ) {}
    public function getExtremePosition( $bGetMax ,$location ) {}
    public function getCardsOfType( $type, $type_arg=null ) {}
    public function getCardsOfTypeInLocation( $type, $type_arg=null, $location, $location_arg = null ) : array { return [];}
    public function shuffle( $location ) {}

    public $autoreshuffle = true;
}

class feException extends Exception {

} 

class APP_GameAction {
    public function isArg(string $argName) : bool { return true;}
    public function getArg(string $argName, string $argType, bool $mandatory = false, $default = null, array $argTypeDetails = [], bool $canFail = false ) {}
    public function trace(string $massage) : void {}
    public function setAjaxMode() : void {}
    public function ajaxResponse() : void {}
}

class game_view {
    public static function _(string $message) : string { return ""; }
}

const AT_int = "";
const AT_posint = "";
const AT_float = "";
const AT_bool = "";
const AT_enum = "";
const AT_alphanum = "";
const AT_numberlist = "";
const AT_base64 = "";

function clienttranslate() {}
function bga_rand(int $from, int $to) : int { return 0; }
function totranslate(string $str) : string { return ""; }

const APP_GAMEMODULE_PATH = "";
const APP_BASE_PATH = "";