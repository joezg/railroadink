<?php

include "/home/jurica/projects/bga/railroadink/modules/RRIBoard.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIField.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIGraph.php";
include "/home/jurica/projects/bga/railroadink/modules/RRINode.php";
include "/home/jurica/projects/bga/railroadink/material.inc.php";

class railroadink {
    const EXPANSION_BASE_ONLY_OPTION_VALUE = 1;
    const EXPANSION_RIVER_OPTION_VALUE = 2;
    const EXPANSION_LAKE_OPTION_VALUE = 3;
    const EXPANSION_LAVA_OPTION_VALUE = 4;
    const EXPANSION_METEOR_OPTION_VALUE = 5;
}

xdebug_break();
$board1Fields = [    
[
    "x"=>0,
"y"=>0,
"route"=>3,
"route_type"=>2,
"is_flipped"=>0,
"rotate"=>-2,
"round"=>NULL,
"is_last_meteor"=>NULL
],

[
    "x"=>0,
"y"=>1,
"route"=>1,
"route_type"=>2,
"is_flipped"=>0,
"rotate"=>0,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>0,
"y"=>2,
"route"=>0,
"route_type"=>2,
"is_flipped"=>0,
"rotate"=>0,
"round"=>NULL,
"is_last_meteor"=>NULL
],

[
    "x"=>0,
"y"=>3,
"route"=>2,
"route_type"=>2,
"is_flipped"=>0,
"rotate"=>0,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>1,
"y"=>1,
"route"=>1,
"route_type"=>1,
"is_flipped"=>0,
"rotate"=>0,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>1,
"y"=>2,
"route"=>0,
"route_type"=>0,
"is_flipped"=>0,
"rotate"=>-3,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>2,
"y"=>1,
"route"=>0,
"route_type"=>0,
"is_flipped"=>0,
"rotate"=>0,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>2,
"y"=>2,
"route"=>2,
"route_type"=>0,
"is_flipped"=>0,
"rotate"=>1,
"round"=>1,
"is_last_meteor"=>NULL
],

[
    "x"=>6,
"y"=>1,
"route"=>8,
"route_type"=>0,
"is_flipped"=>1,
"rotate"=>5,
"round"=>1,
"is_last_meteor"=>NULL
],
];

$board1 = new RRIBoard(12, 2);

$board1->addFields($board1Fields);

$score1 = $board1->getScore(3);

var_dump($score1);
