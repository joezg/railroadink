<?php

include "/home/jurica/projects/bga/railroadink/modules/RRIBoard.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIField.php";
include "/home/jurica/projects/bga/railroadink/modules/RRIGraph.php";
include "/home/jurica/projects/bga/railroadink/modules/RRINode.php";
include "/home/jurica/projects/bga/railroadink/material.inc.php";

xdebug_break();
$board1Fields = [  
    [
        "x" => 1,
        "y" => 4,
        "route" => 0,
        "route_type" => 0,
        "is_flipped" => 0,
        "rotate" => 2,
        "round" => NULL,
        "is_last_meteor" => NULL
    ],
    [
        "x" => 1,
        "y" => 5,
        "route" => 7,
        "route_type" => 0,
        "is_flipped" => 0,
        "rotate" => 0,
        "round" => NULL,
        "is_last_meteor" => NULL
    ],
    [
        "x" => 1,
        "y" => 6,
        "route" => 4,
        "route_type" => 0,
        "is_flipped" => 0,
        "rotate" => 1,
        "round" => NULL,
        "is_last_meteor" => NULL
    ],
    [
        "x" => 2,
        "y" => 3,
        "route" => 3,
        "route_type" => 1,
        "is_flipped" => 0,
        "rotate" => 0,
        "round" => NULL,
        "is_last_meteor" => NULL
    ],
    [
        "x" => 2,
        "y" => 4,
        "route" => 1,
        "route_type" => 0,
        "is_flipped" => 0,
        "rotate" => 0,
        "round" => NULL,
        "is_last_meteor" => NULL
    ],
    [
        "x" => 3,
        "y" => 3,
        "route" => 5,
        "route_type" => 4,
        "is_flipped" => 0,
        "rotate" => 0,
        "round" => -1,
        "is_last_meteor" => 0
    ],

];


$board1 = new RRIBoard(12, 4);
$board1->addFields($board1Fields);

$fields = $board1->getAvailableFields();
$availableFields = array_map(function ($field) {
    $field["possibleRoutes"] = RRIField::getPossibleRoutes($field, 4);
    return $field;
}, $fields);
	
var_dump($availableFields);
