<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 * 
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * railroadink.game.php
 *
 * This is the main file for your game logic.
 *
 * In this PHP file, you are going to defines the rules of the game.
 *
 */


require_once(APP_GAMEMODULE_PATH . 'module/table/table.game.php');
require_once('modules/RRIField.php');
require_once('modules/RRINode.php');
require_once('modules/RRIGraph.php');
require_once('modules/RRIBoard.php');
require_once('modules/RRIStats.php');
require_once('modules/DebugStudio.php');

class railroadink extends Table
{

    const GAME_ROUND_GLOBAL = 'game_round';
    const TOTAL_ROUNDS_GLOBAL = 'total_rounds';
    const EXPANSION_BLUE_OPTION = 'expansion_blue_option';
    const EXPANSION_RED_OPTION = 'expansion_red_option';
    const EDITION_OPTION = 'edition_option';
    const EXPANSION_BASE_ONLY_OPTION_VALUE = 1;
    const EXPANSION_RIVER_OPTION_VALUE = 2;
    const EXPANSION_LAKE_OPTION_VALUE = 3;
    const EXPANSION_LAVA_OPTION_VALUE = 4;
    const EXPANSION_METEOR_OPTION_VALUE = 5;
    const MOVE_TYPE_ROUTE = 'route';
    const MOVE_TYPE_METEOR_ALLOWED = 'meteor-allowed'; 
    const MOVE_TYPE_METEOR_PREVENTED = 'meteor-prevented'; 
    const MOVE_TYPE_VOLCANO = 'volcano';

    function __construct()
    {
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();

        $this->initGameStateLabels(array(
            self::GAME_ROUND_GLOBAL => 10,
            self::TOTAL_ROUNDS_GLOBAL => 11,
            self::EXPANSION_BLUE_OPTION => 100,
            self::EDITION_OPTION => 101,
            self::EXPANSION_RED_OPTION => 102,
        ));
    }

    protected function getGameName()
    {
        // Used for translations and stuff. Please do not modify.
        return "railroadink";
    }

    /*
        ███████╗███████╗████████╗██╗   ██╗██████╗ 
        ██╔════╝██╔════╝╚══██╔══╝██║   ██║██╔══██╗
        ███████╗█████╗     ██║   ██║   ██║██████╔╝
        ╚════██║██╔══╝     ██║   ██║   ██║██╔═══╝ 
        ███████║███████╗   ██║   ╚██████╔╝██║     
        ╚══════╝╚══════╝   ╚═╝    ╚═════╝ ╚═╝     
                                          
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = array())
    {
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $gameinfos = $this->getGameinfos();
        $default_colors = $gameinfos['player_colors'];

        $expansion = $this->getExpansionRouteType();
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        $boardValues = [];
        foreach ($players as $player_id => $player) {
            $color = array_shift($default_colors);
            $values[] = "('" . $player_id . "','$color','" . $player['player_canal'] . "','" . addslashes($player['player_name']) . "','" . addslashes($player['player_avatar']) . "')";

            if ($expansion == self::EXPANSION_LAVA_OPTION_VALUE){
                // central volcano
                $boardValues[] = "('$player_id', 3, 3, 5, 4, 0, 0, -1, 0)";
            }
        }
        $sql .= implode(',', $values);
        $this->DbQuery($sql);

        if ($expansion == self::EXPANSION_LAVA_OPTION_VALUE){
            $sql = "INSERT INTO board (player_id, x, y, route, route_type, is_flipped, rotate, round, is_last_meteor) VALUES ";
            $sql .= implode(',', $boardValues);
            $this->DbQuery($sql);
        }

        $this->reattributeColorsBasedOnPreferences($players, $gameinfos['player_colors']);
        $this->reloadPlayersBasicInfos();

        /************ Start the game initialization *****/

        // Init global values with their initial values
        $this->setGameStateInitialValue( self::GAME_ROUND_GLOBAL, 0 );
        $totalNumberOfRounds = $this->getTotalNumberOfRounds();
        $this->setGameStateInitialValue( self::TOTAL_ROUNDS_GLOBAL, $totalNumberOfRounds );

        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        $this->initStat("player", RRIStats::PLAYED_RIVER_EXPANSION, $this->getGameStateValue(self::EXPANSION_BLUE_OPTION) == self::EXPANSION_RIVER_OPTION_VALUE);
        $this->initStat("player", RRIStats::PLAYED_LAKE_EXPANSION, $this->getGameStateValue(self::EXPANSION_BLUE_OPTION) == self::EXPANSION_LAKE_OPTION_VALUE);
        $this->initStat("player", RRIStats::PLAYED_LAVA_EXPANSION, $this->getGameStateValue(self::EXPANSION_RED_OPTION) == self::EXPANSION_LAVA_OPTION_VALUE);
        $this->initStat("player", RRIStats::PLAYED_METEOR_EXPANSION, $this->getGameStateValue(self::EXPANSION_RED_OPTION) == self::EXPANSION_METEOR_OPTION_VALUE);

        // TODO: setup the initial game situation here

        /************ End of the game initialization *****/
    }

    /*
         ██████╗ ███████╗████████╗     █████╗ ██╗     ██╗         ██████╗  █████╗ ████████╗ █████╗ 
        ██╔════╝ ██╔════╝╚══██╔══╝    ██╔══██╗██║     ██║         ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗
        ██║  ███╗█████╗     ██║       ███████║██║     ██║         ██║  ██║███████║   ██║   ███████║
        ██║   ██║██╔══╝     ██║       ██╔══██║██║     ██║         ██║  ██║██╔══██║   ██║   ██╔══██║
        ╚██████╔╝███████╗   ██║       ██║  ██║███████╗███████╗    ██████╔╝██║  ██║   ██║   ██║  ██║
         ╚═════╝ ╚══════╝   ╚═╝       ╚═╝  ╚═╝╚══════╝╚══════╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝

        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array();

        $current_player_id = $this->getCurrentPlayerId();    // !! We must only return informations visible by this player !!

        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score, player_is_multiactive is_multiactive, used_volcano FROM player ";
        $players = $this->getCollectionFromDb($sql);
        
        $result['dice'] = $this->getCurrentDiceValues();
        $result["round"] = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        $result["totalRounds"] = $this->getGameStateValue(self::TOTAL_ROUNDS_GLOBAL);
        
        $result["boards"] = [];
        $boards = $this->getPlayerBoards();

        $isCurrentPlayerActive = false;

        if (array_key_exists($current_player_id, $players)){
            $isCurrentPlayerActive = $players[$current_player_id]["is_multiactive"] == 1;
        }
        
        foreach ($boards as $playerId => $board) {
            $roundToMarkAsCurrent = $result["round"];

            if ($playerId != $current_player_id){
                $board->removeUnconfirmedFields();

                $isPlayerActive = $players[$playerId]["is_multiactive"] == 1;
                
                if ($isCurrentPlayerActive || $isPlayerActive){
                    $board->removeFieldsForRound($this->getGameStateValue(self::GAME_ROUND_GLOBAL));
                    $roundToMarkAsCurrent = +$roundToMarkAsCurrent - 1;
                }
            }

            $serializedBoard = $board->serialize($roundToMarkAsCurrent);
            $players[$playerId]["board"] = $serializedBoard;
            $players[$playerId]["score"] = $serializedBoard["score"]["total"];
        }
        $result['players'] = $players;
        
        $usedDice = $this->getDiceUsage();
        $result["usedDice"] =  $usedDice;
        $result["usedSpecial"] = $this->getSpecialUsage();

        $result["edition"] = $this->getGameStateValue(self::EDITION_OPTION);
        $result["expansion"] = $this->getExpansionRouteType();
        
        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        $totalRounds = $this->getGameStateValue(self::TOTAL_ROUNDS_GLOBAL);
        return (($round - 1) / $totalRounds) * 100;
    }

// ██╗   ██╗████████╗██╗██╗     ██╗████████╗██╗   ██╗    ███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
// ██║   ██║╚══██╔══╝██║██║     ██║╚══██╔══╝╚██╗ ██╔╝    ██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
// ██║   ██║   ██║   ██║██║     ██║   ██║    ╚████╔╝     █████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
// ██║   ██║   ██║   ██║██║     ██║   ██║     ╚██╔╝      ██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
// ╚██████╔╝   ██║   ██║███████╗██║   ██║      ██║       ██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
//  ╚═════╝    ╚═╝   ╚═╝╚══════╝╚═╝   ╚═╝      ╚═╝       ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    function getTotalNumberOfRounds(){
        return $this->isExpansionIncluded() ? 6 : 7;
    }

    function isExpansionIncluded() {
        if ($this->getEdition() == "blue"){
            return $this->getGameStateValue(self::EXPANSION_BLUE_OPTION) != self::EXPANSION_BASE_ONLY_OPTION_VALUE;
        }
        
        if ($this->getEdition() == "red"){
            return $this->getGameStateValue(self::EXPANSION_RED_OPTION) != self::EXPANSION_BASE_ONLY_OPTION_VALUE;
        }

        return false;
    }

    function resetDiceUse() {
        $sql = "DELETE FROM player_die_use";
        $this->DbQuery($sql);
    }
    
    function resetMoves() {
        $sql = "DELETE FROM move";
        $this->DbQuery($sql);
    }

    function getEdition() {
        $edition = $this->getGameStateValue(self::EDITION_OPTION);

        switch ($edition) {
            case 1:
                return "blue";
                break;
            
            case 2:
                return "red";
                break;
        }

        return "blue";
    }

    function getExpansionRouteType(){
        $expansionBlue = $this->getGameStateValue(self::EXPANSION_BLUE_OPTION);
        $expansionRed = $this->getGameStateValue(self::EXPANSION_RED_OPTION);

        switch ($expansionBlue) {
            case self::EXPANSION_RIVER_OPTION_VALUE:
                return RRIField::TYPE_RIVER;
            case self::EXPANSION_LAKE_OPTION_VALUE:
                return RRIField::TYPE_LAKE;
        }

        switch ($expansionRed) {
            case self::EXPANSION_LAVA_OPTION_VALUE:
                return RRIField::TYPE_LAVA;
            case self::EXPANSION_METEOR_OPTION_VALUE:
                return RRIField::TYPE_METEOR;
            
        }

        return null;
    }

    function rollDiceAndPersist()
    {

        // first clear data in dice table
        $sql = "DELETE FROM dice";
        $this->DbQuery($sql);


        // insert new values for dice
        $sql = "INSERT INTO dice (id, route, route_type) VALUES";
        $values = array();
        for ($id = 0; $id < 4; $id++) {
            if ($id < 3) {
                $values[] = "('" . $id . "'," . bga_rand(0, 5) . ", 0)";
            } else {
                $values[] = "('" . $id . "'," . bga_rand(6, 8) . ", 0)";
            }
        }

        $expansionRouteType = $this->getExpansionRouteType();
        if ($expansionRouteType == RRIField::TYPE_RIVER) {
            for ($id = 4; $id < 6; $id++) {
                $value = bga_rand(0, 5);
                if ($value > 3) {
                    $value = 3;
                }
                $values[] = "('" . $id . "'," . $value . ", ".$expansionRouteType.")";
            }
        }

        if ($expansionRouteType == RRIField::TYPE_LAKE) {
            for ($id = 4; $id < 6; $id++) {
                $value = bga_rand(0, 5);
                $values[] = "('" . $id . "'," . $value . ", ".$expansionRouteType.")";
            }
        }
        
        if ($expansionRouteType == RRIField::TYPE_METEOR) {
            for ($id = 4; $id < 6; $id++) {
                $value = bga_rand(0, 5);
                
                if ($id == 4){
                    if ($value < 3) {
                        $value = 0;
                    } elseif ($value < 5) {
                        $value = 1;
                    } else {
                        $value = 2;
                    }
                } else {
                    if ($value == 5) {
                        $value = 4;
                    }

                    $value += 3;
                }
                $values[] = "('" . $id . "'," . $value . ", ".$expansionRouteType.")";
            }
        }

        if ($expansionRouteType == RRIField::TYPE_LAVA) {
            for ($id = 4; $id < 6; $id++) {
                $value = bga_rand(0, 5);
                if ($value > 0) {
                    $value--;
                }
                $values[] = "('" . $id . "'," . $value . ", ".$expansionRouteType.")";
            }
        }

        $sql .= implode(',', $values);
        $this->DbQuery($sql);
    }

    function getCurrentDiceValues()
    {
        $sql = "SELECT id, route, route_type routeType FROM dice";
        return $this->getObjectListFromDB($sql);
    }

    function getDiceUsage()
    {
        $sql = "SELECT die_id, player_id FROM player_die_use";
        $dieUsage = $this->getObjectListFromDB($sql);

        $usedDice = [];
        foreach ($dieUsage as $usage) {
            $dieId = intval($usage["die_id"]);
            $usedDice[$usage["player_id"]][$dieId] = [ "id" => intval($usage["die_id"]) ];
        }

        return $usedDice;
    }
    
    function getSpecialUsage()
    {
        $sql = "SELECT special_id, player_id, round FROM player_special_use";
        $specialUsage = $this->getObjectListFromDB($sql);
        
        $usedSpecial = [];
        foreach ($specialUsage as $usage) {
            $specialId = intval($usage["special_id"]);
            if ($usage["round"] == null || $usage["round"] == $this->getGameStateValue(self::GAME_ROUND_GLOBAL)){
                $usedSpecial[$usage["player_id"]]["currentRoundUsed"] = true;
            }
            $usedSpecial[$usage["player_id"]]["routes"][$specialId] = [ 
                "id" => intval($usage["special_id"]),
                "round" => intval($usage["round"])
            ];
        }

        return $usedSpecial;
    }

    function useDie($dieId, $playerId){
        $sql = "INSERT INTO player_die_use (die_id, player_id) VALUES (".$dieId.", ".$playerId.")";
        $this->DbQuery($sql);
    }
    
    function useSpecial($specialId, $playerId, $round = "null"){
        $sql = "INSERT INTO player_special_use (special_id, player_id, round) VALUES (".$specialId.", ".$playerId.", $round)";
        $this->DbQuery($sql);
    }

    private function getPlayerBoards($playerId = null)
    {
        $sql = "SELECT player_id, x, y, route, route_type, is_flipped, rotate, `round`, is_last_meteor FROM player LEFT OUTER JOIN board USING (player_id)";
        if ($playerId !== null) {
            $sql .= " WHERE player_id = " . $playerId;
        }
        $fields = $this->getObjectListFromDB($sql);

        $boardsByPlayer = [];

        $expansion = $this->getExpansionRouteType();

        foreach ($fields as $field) {
            if (!array_key_exists($field["player_id"], $boardsByPlayer)) {
                $board = new RRIBoard($field["player_id"], $expansion);
                $boardsByPlayer[$field["player_id"]] = $board;
            } else {
                $board = $boardsByPlayer[$field["player_id"]];
            }
            if ($field["route"] !== null) {
                $board->addField($field);
            }
        }

        if ($playerId !== null) {
            return $boardsByPlayer[$playerId];
        }

        return $boardsByPlayer;
    }

    function getLastPlayerMoves($playerId){
        $sql = <<<SQL
            SELECT x, y, die_id, special_id, move_id, move_type, replaced_route, replaced_route_type, replaced_is_flipped, replaced_rotate, replaced_round, replaced_is_last_meteor FROM move
            WHERE move_id = (SELECT max(move_id) FROM move WHERE player_id = $playerId)
            AND player_id = $playerId
SQL;
        return $this->getObjectListFromDB($sql);
    }
    
    function getPlayerMoves($playerId){
        $sql = <<<SQL
            SELECT x, y, die_id, special_id, move_id, move_type, replaced_route, replaced_route_type, replaced_is_flipped, replaced_rotate, replaced_round, replaced_is_last_meteor FROM move
            WHERE player_id = $playerId
SQL;
        return $this->getObjectListFromDB($sql);
    }

    private function replaceFieldOnPlayerBoard($playerId, $route, $routeType, $rotation, $flipped, $x, $y, $isLastMeteor = 'null'){
        $this->DbQuery(
            "UPDATE board SET route = $route, route_type = $routeType, is_flipped = $flipped, rotate = $rotation, is_last_meteor = $isLastMeteor, round = null"
            ." WHERE player_id = $playerId AND x = $x AND y = $y"
        );

    }

    private function addFieldToPlayerBoard($playerId, $route, $routeType, $rotation, $flipped, $x, $y, $isLastMeteor = null, $round = null)
    {
        $sql = "INSERT INTO board (player_id, x, y, route, route_type, is_flipped, rotate, is_last_meteor, round)";

        $values = [
            $playerId, $x, $y, $route, $routeType, $flipped, $rotation, $isLastMeteor > 0 ? $isLastMeteor : 'null', $round === null ? 'null' : $round
        ];

        $sql .= "VALUES (" . implode(",", $values) . ")";

        if ($isLastMeteor == 1){
            $currentField = $this->getObjectFromDB("SELECT * FROM board WHERE player_id = $playerId AND x = $x AND y = $y");

            if ($currentField){
                return $this->replaceFieldOnPlayerBoard($playerId, $route, $routeType, $rotation, $flipped, $x, $y, 1);
            }
        }

        $this->DbQuery($sql);
    }

    private function canPlayerFinish($usedDice, $playerId){
        $dieIds = array_column($usedDice, "id");

        $usedAllWhite = sizeof(array_intersect($dieIds, [0, 1, 2, 3])) === 4;

        if (!$usedAllWhite){
            $unusedDice = array_diff([0, 1, 2, 3], $dieIds);

            $dice = $this->getCurrentDiceValues();

            foreach ($unusedDice as $dieId) {
                $board = $this->getPlayerBoards($playerId);
                $availableFields = $this->getAvailableFields($board);
                foreach ($availableFields as $availableField) {
                    foreach ($availableField["possibleRoutes"] as $possibleRoute) {
                        if (sizeof($possibleRoute["normalRotations"]) != 0 || sizeof($possibleRoute["normalRotations"]) != 0){
                            if ($possibleRoute["route"] == $dice[$dieId]["route"] 
                                && $possibleRoute["routeType"] == $dice[$dieId]["routeType"]){
                                    return false;
                                }
                        }
                    }
                }
            }
        }

        if ($this->getExpansionRouteType() === self::EXPANSION_LAVA_OPTION_VALUE){
            $usedEnoughLava = sizeof(array_intersect($dieIds, [4, 5])) >= 1;

            if (!$usedEnoughLava){
                return false;
            }
        }


        return true;
    }

    private function isPlayerActive($playerId){
        return array_search($playerId, $this->gamestate->getActivePlayerList()) !== false;
    }

    private function getAllInactivePlayers(){
        $sql = "SELECT player_id, player_name FROM player WHERE player_is_multiactive = 0";
        
        return $this->getObjectListFromDB( $sql );
    }

    private function updateCurrentRoundFields($playerId){
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);

        $sql = "UPDATE board SET round = ".$round." WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
        
        $sql = "UPDATE player_special_use SET round = ".$round." WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
    }

    private function resetPlayerRound($playerId){
        $sql = "DELETE FROM player_die_use WHERE player_id = ".$playerId;
        $this->DbQuery($sql);
        
        $sql = "DELETE FROM move WHERE player_id = ".$playerId;
        $this->DbQuery($sql);

        $sql = "DELETE FROM board WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
        
        $sql = "DELETE FROM player_special_use WHERE player_id = ".$playerId." AND round IS NULL";
        $this->DbQuery($sql);
        
        $sql = "UPDATE player SET used_volcano = 0 WHERE player_id = ".$playerId;
        $this->DbQuery($sql);
    } 
    
    private function resetPlayerLastMove($playerId, $dieId, $specialId, $x, $y, $moveId){
        if ($dieId != null){
            $sql = "DELETE FROM player_die_use WHERE player_id = $playerId AND die_id = $dieId";
            $this->DbQuery($sql);
        }

        if ($x !== null && $y !== null){
            $sql = "DELETE FROM board WHERE player_id = $playerId AND x = $x and y = $y";
            $this->DbQuery($sql);
        }
        
        if ($specialId != null){
            $sql = "DELETE FROM player_special_use WHERE player_id = $playerId AND special_id = $specialId";
            $this->DbQuery($sql);
        }

        $sql = "DELETE FROM move WHERE player_id = $playerId AND move_id = $moveId";
        $this->DbQuery($sql);
    } 

    function dbGetScore($player_id) {
        return $this->getUniqueValueFromDB("SELECT player_score FROM player WHERE player_id='$player_id'");
    }

    function dbSetScore($player_id, $count, $errors) {
        $auxScore = $count + $errors["total"];
        $this->DbQuery("UPDATE player SET player_score='$count', player_score_aux = '$auxScore' WHERE player_id='$player_id'");
    }

    function markVolcanoUsed($playerId){
        $sql = "UPDATE player SET used_volcano = 1 WHERE player_id = $playerId";
        $this->DbQuery($sql);
    }

    function resetVolcanoUse($playerId = null){
        $sql = "UPDATE player SET used_volcano = 0";

        if ($playerId !== null){
            $sql .= " WHERE player_id = $playerId";
        }

        $this->DbQuery($sql);
    }

    function checkVolcanoUsed($playerId){
        $sql = "SELECT used_volcano FROM player WHERE player_id = $playerId";
        return $this->getUniqueValueFromDB($sql) == "1";
    }

    function getAvailableFields($board){
        $expansion = $this->getExpansionRouteType();
        $fields = $board->getAvailableFields();
        $availableFields = array_map(function ($field) use ($expansion) {
            $field["possibleRoutes"] = array_values(array_filter(RRIField::getPossibleRoutes($field, $expansion), function($route){
                return sizeof($route["normalRotations"]) > 0 || sizeof($route["flippedRotations"]) > 0;
            }));
            return $field;
        }, $fields);

        return $availableFields;
    }

    function checkIfLegal($board, $field){
        $availableFields = $this->getAvailableFields($board);
        $key = $field->getX().$field->getY();
        
        if (!isset($availableFields[$key])){
           return false;
        }

        $possibleRoutes = $availableFields[$key]["possibleRoutes"];

        foreach ($possibleRoutes as $route) {
            if ($route["route"] == $field->getRoute() && $route["routeType"] == $field->getRouteType()){
                return true;
            }
        }

        return false;
    }

// ██████╗ ██╗      █████╗ ██╗   ██╗███████╗██████╗      █████╗  ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
// ██╔══██╗██║     ██╔══██╗╚██╗ ██╔╝██╔════╝██╔══██╗    ██╔══██╗██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
// ██████╔╝██║     ███████║ ╚████╔╝ █████╗  ██████╔╝    ███████║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
// ██╔═══╝ ██║     ██╔══██║  ╚██╔╝  ██╔══╝  ██╔══██╗    ██╔══██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
// ██║     ███████╗██║  ██║   ██║   ███████╗██║  ██║    ██║  ██║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
// ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
    
    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in railroadink.action.php)
    */

    function decideMeteor($x, $y){
        $this->checkAction('decideMeteor');

        $playerId = $this->getCurrentPlayerId();

        $board= $this->getPlayerBoards($playerId);
        $dice = $this->getCurrentDiceValues();
        
        $possibleTargets = $board->calculateMeteorTargets($dice[4]["route"], $dice[5]["route"]);

        $found = false;
        foreach ($possibleTargets as $target) {
            if ($target["x"] == $x && $target["y"] == $y){
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new BgaUserException( self::_('This meteor target is not valid') );
        }

        $isReplaced = $board->isFieldExists($x, $y);
        $replacedField = null;
        if ($isReplaced){
            $replacedField = $board->getField($x, $y);
        }

        $this->DbQuery("UPDATE board SET is_last_meteor = null WHERE player_id = $playerId AND is_last_meteor = 2");
        $this->DbQuery("UPDATE board SET is_last_meteor = 2 WHERE player_id = $playerId AND is_last_meteor = 1");

        $isLastMeteor = 1;
        $this->addFieldToPlayerBoard($playerId, 0, self::EXPANSION_METEOR_OPTION_VALUE, 0, 0, $x, $y, $isLastMeteor);

        $board = $this->getPlayerBoards($playerId);
        $this->saveMove($playerId, $board->getField($x, $y), null, null, $replacedField, 'meteor-allowed');

        $this->notifyPlayer($playerId, "meteorStrikes", "Meteor struck the earth", [
            "board" => $board->serialize()
        ]);

        $this->gamestate->nextPrivateState( $playerId, "useDice" );

    }
    
    function allowMeteor(){
        $this->checkAction('allowMeteor');

        $playerId = $this->getCurrentPlayerId();

        $board= $this->getPlayerBoards($playerId);
        $dice = $this->getCurrentDiceValues();
        
        $possibleTargets = $board->calculateMeteorTargets($dice[4]["route"], $dice[5]["route"]);

        if (sizeof($possibleTargets) > 1){
            throw new BgaUserException( self::_('You cannot just allow meteor, but need to select the field') );
        }

        $target = $possibleTargets[0];

        $isReplaced = $board->isFieldExists($target["x"], $target["y"]);
        $replacedField = null;
        if ($isReplaced){
            $replacedField = $board->getField($target["x"], $target["y"]);
        }

        $this->DbQuery("UPDATE board SET is_last_meteor = null WHERE player_id = $playerId AND is_last_meteor = 2");
        $this->DbQuery("UPDATE board SET is_last_meteor = 2 WHERE player_id = $playerId AND is_last_meteor = 1");

        $isLastMeteor = 1;
        $this->addFieldToPlayerBoard($playerId, 0, self::EXPANSION_METEOR_OPTION_VALUE, 0, 0, $target["x"], $target["y"], $isLastMeteor);

        $board = $this->getPlayerBoards($playerId);
        $this->saveMove($playerId, $board->getField($target["x"], $target["y"]), null, null, $replacedField, 'meteor-allowed');

        $this->notifyPlayer($playerId, "meteorStrikes", clienttranslate("Meteor struck the earth"), [
            "board" => $board->serialize()
        ]);

        $this->gamestate->nextPrivateState( $playerId, "useDice" );

    }

    function decidePrevent($route){
        $this->checkAction('decidePrevent');
        $playerId = $this->getCurrentPlayerId();

        $specialRouteUsage = $this->getSpecialUsage();
        $usedSpecial = isset($specialRouteUsage[$playerId]) ? $specialRouteUsage[$playerId] : [];
            
        if (isset($usedSpecial["routes"][$route])){
            throw new BgaUserException( self::_('This special route is already used') );
        }
        if (isset($usedSpecial["currentRoundUsed"]) && $usedSpecial["currentRoundUsed"] ){
            throw new BgaUserException( self::_('Special route is already used in this round') );
        }
        if (isset($usedSpecial["routes"]) && sizeof($usedSpecial["routes"]) === 3){
            throw new BgaUserException( self::_('Three special routes already used in a game') );
        }

        $this->useSpecial($route, $playerId);

        $this->saveMove($playerId, null, null, $route, null, 'meteor-prevented');

        $this->gamestate->nextPrivateState( $playerId, "useDice" );
    }

    function doneUseDice(){
        $this->checkAction('doneUseDice');
        
        $playerId = $this->getCurrentPlayerId();
        $playerName = $this->getCurrentPlayerName();
        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$playerId]) ? $diceUsage[$playerId] : [];
        
        if (!$this->canPlayerFinish($usedDice, $playerId)) {
            $msg = self::_('You must use all dice if possible');
            if ($this->getExpansionRouteType() !== self::EXPANSION_BASE_ONLY_OPTION_VALUE) {
                $msg = self::_('You must use all white dice if possible');
                if ($this->getExpansionRouteType() === self::EXPANSION_LAVA_OPTION_VALUE) {
                    $msg = self::_('You must use all white dice if possible and must use at least one lava die');
                }
            }

            throw new BgaUserException( $msg );
        }

        $this->updateCurrentRoundFields($playerId);

        $this->notifyAllPlayers("playerFinished", clienttranslate( '${player_name} finished drawing routes'), [
            "player_name" => $playerName,
            "playerId" => $playerId
        ] );

        $expansion = $this->getExpansionRouteType();
        $boards = $this->getPlayerBoards();
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);

        if ($expansion == self::EXPANSION_LAKE_OPTION_VALUE){
            $board = $boards[$playerId];
            while (($toFill = $board->findFieldSurroundedBy("L", 3)) !== null) {
                $this->addFieldToPlayerBoard($playerId, 6, self::EXPANSION_LAKE_OPTION_VALUE, 0, 0, $toFill["x"], $toFill["y"], null, $round);
                $board->addField([
                    "route" => 6,
                    "route_type" => self::EXPANSION_LAKE_OPTION_VALUE,
                    "rotate" => 0,
                    "is_flipped" => 0,
                    "x" => $toFill["x"],
                    "y" => $toFill["y"],
                    "round" => $round
                ]);
            }
        }
        
        $this->notifyPlayer( $playerId, "updatePlayerBoard", clienttranslate('Round number is written on newly drawn pieces. You cannot undo this round any more.'), [
            "board" => $boards[$playerId]->filterLastRoundAndSerialize($round),        
        ] );

        $specialUsage = $this->getSpecialUsage();
        $inactivePlayers = $this->getAllInactivePlayers();
        foreach ($inactivePlayers as $inactivePlayer) {
            if ($inactivePlayer["player_id"] != $playerId){
                $this->notifyPlayer( $playerId, "updatePlayerBoard", clienttranslate('You can now see what ${player_name} has drawn this round'), [
                    "board" => $boards[$inactivePlayer["player_id"]]->filterLastRoundAndSerialize($round),
                    "player_name" => $inactivePlayer["player_name"],
                    "specialUsage" => array_key_exists($inactivePlayer["player_id"], $specialUsage) ? $specialUsage[$inactivePlayer["player_id"]] : null
                ] );

                $this->notifyPlayer( $inactivePlayer["player_id"], "updatePlayerBoard", 'You can now see what ${player_name} has drawn this round', [
                    "board" => $boards[$playerId]->filterLastRoundAndSerialize($round),
                    "player_name" => $playerName,
                    "specialUsage" => array_key_exists($playerId, $specialUsage) ? $specialUsage[$playerId] : null
                ] );
            }
        }

        $this->gamestate->setPlayerNonMultiactive( $playerId, "nextRound" );
    }

    function undoUseDice() {
        $this->checkAction('undoUseDice');
        
        $playerId = $this->getCurrentPlayerId();
        if (!$this->isPlayerActive($playerId)){
            throw new BgaUserException( self::_('You cannot undo drawing routes if you finalize them.') );
        }
        
        $moves = $this->getLastPlayerMoves($playerId);

        if (sizeof($moves) == 0){
            throw new BgaUserException( self::_('You cannot undo previous rounds moves.') );
        }

        $undoVolcano = false;

        $board = $this->getPlayerBoards($playerId);
        $newPrivateTransition = null;
        foreach ($moves as $move) {
            $this->resetPlayerLastMove($playerId, $move["die_id"], $move["special_id"], $move["x"], $move["y"], $move["move_id"]);
            
            if ($move["replaced_route"] !== null){
                $this->addFieldToPlayerBoard($playerId, $move["replaced_route"], $move["replaced_route_type"], $move["replaced_rotate"], $move["replaced_is_flipped"], $move["x"], $move["y"], $move["replaced_is_last_meteor"], $move["replaced_round"]);
            }
            
            if ($move["move_type"] == self::MOVE_TYPE_VOLCANO) {
                $this->resetVolcanoUse($playerId);
                $undoVolcano = true;
            }

            if (in_array($move["move_type"], [self::MOVE_TYPE_METEOR_ALLOWED, self::MOVE_TYPE_METEOR_PREVENTED])) {
                $dice = $this->getCurrentDiceValues();

                if ($dice[5]["route"] == 7){
                    $newPrivateTransition = "decideMeteor";
                } else {
                    $newPrivateTransition = "decideMeteorCancel";
                }
                
                $sql = "UPDATE board SET is_last_meteor = 1 WHERE player_id = $playerId AND is_last_meteor = 2";
                $this->DbQuery($sql);
            }

        }

        $board = $this->getPlayerBoards($playerId);

        $availableFields = $this->getAvailableFields($board);

        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$playerId]) ? $diceUsage[$playerId] : [];

        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        $this->notifyPlayer( $playerId, "undoUseDice", "", [
            "board" => $board->serialize($round),
            "availableFields" => $availableFields,
            "usedSpecial" => $this->getSpecialUsage(),
            "usedDice" => $this->getDiceUsage(),
            "undoVolcano" => $undoVolcano,
            "canFinish" => $this->canPlayerFinish($usedDice, $playerId)
        ] );

        if ($newPrivateTransition !== null) {
            $this->gamestate->nextPrivateState($playerId, $newPrivateTransition);
        }
        
    }
    
    function restartUseDice() {
        $this->checkAction('restartUseDice');

        $playerId = $this->getCurrentPlayerId();
        if (!$this->isPlayerActive($playerId)){
            throw new BgaUserException( self::_('You cannot undo drawing routes if you finalize them.') );
        }

        $moves = $this->getPlayerMoves($playerId);
        $board = $this->getPlayerBoards($playerId);

        $this->resetPlayerRound($playerId);

        foreach ($moves as $move) {
            if (in_array($move["move_type"], [self::MOVE_TYPE_ROUTE, self::MOVE_TYPE_METEOR_ALLOWED, self::MOVE_TYPE_VOLCANO])){
                if ($move["replaced_route"] !== null && $move["replaced_round"] !== null){
                    $this->addFieldToPlayerBoard($playerId, $move["replaced_route"], $move["replaced_route_type"], $move["replaced_rotate"], $move["replaced_is_flipped"], $move["x"], $move["y"], $move["replaced_is_last_meteor"], $move["replaced_round"]);
                }
            }
        }

        if ($this->getExpansionRouteType() == self::EXPANSION_METEOR_OPTION_VALUE){
            $sql = "UPDATE board SET is_last_meteor = 1 WHERE player_id = $playerId AND is_last_meteor = 2";
            $this->DbQuery($sql);
        }

        $board = $this->getPlayerBoards($playerId);

        $availableFields = $this->getAvailableFields($board);

        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$playerId]) ? $diceUsage[$playerId] : [];

        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        $this->notifyPlayer( $playerId, "restartUseDice", "", [
            "board" => $board->serialize($round, $this->getExpansionRouteType()),
            "availableFields" => $availableFields,
            "usedSpecial" => $this->getSpecialUsage(),
            "canFinish" => $this->canPlayerFinish($usedDice, $playerId)
        ] );

        if ($this->getExpansionRouteType() == self::EXPANSION_METEOR_OPTION_VALUE){
            $dice = $this->getCurrentDiceValues();

            if ($dice[5]["route"] == 7){
                return $this->gamestate->nextPrivateState($playerId, "decideMeteor");
            } else {
                return $this->gamestate->nextPrivateState($playerId, "decideMeteorCancel");
            }
        }
    }
    
    function drawRoute($route, $routeType, $rotation, $flipped, $x, $y, $dieId, $specialId)
    {
        // Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        $this->checkAction('drawRoute');
        
        $player_id = $this->getCurrentPlayerId();

        $board = $this->getPlayerBoards($player_id);
        $fieldRaw = [
            "route" => $route,
            "route_type" => $routeType,
            "rotate" => $rotation,
            "is_flipped" => $flipped,
            "x" => $x,
            "y" => $y,
            "round" => null
        ];
        $field = new RRIField($fieldRaw);
        
        if (!$this->checkIfLegal($board, $field)){
            throw new BgaUserException( self::_('This placement is not legal') );
        }
        
        $diceUsage = $this->getDiceUsage();
        $usedDice = isset($diceUsage[$player_id]) ? $diceUsage[$player_id] : [];
        if ($dieId !== null){
            if (isset($usedDice[$dieId])){
                throw new BgaUserException( self::_('This die is already used') );
            }
        }
        
        $specialRouteUsage = $this->getSpecialUsage();
        $usedSpecial = isset($specialRouteUsage[$player_id]) ? $specialRouteUsage[$player_id] : [];
        if ($specialId !== null) {
            if (isset($usedSpecial["routes"][$specialId])){
                throw new BgaUserException( self::_('This special route is already used') );
            }
            if (isset($usedSpecial["currentRoundUsed"]) && $usedSpecial["currentRoundUsed"] ){
                throw new BgaUserException( self::_('Special route is already used in this round') );
            }
            if (isset($usedSpecial["routes"]) && sizeof($usedSpecial["routes"]) === 3){
                throw new BgaUserException( self::_('Three special routes already used in a game') );
            }
        }

        $moveType = self::MOVE_TYPE_ROUTE;

        if ($specialId == null && $dieId == null){
            if ($this->getExpansionRouteType() != self::EXPANSION_LAVA_OPTION_VALUE){
                throw new BgaUserException( "Not possible to draw a route without die or special id if expansion is not lava" );
            }

            if ($this->checkVolcanoUsed($player_id)){
                throw new BgaUserException( self::_('You already drew volcano this round') );
            }
            $this->markVolcanoUsed($player_id);
            $moveType = self::MOVE_TYPE_VOLCANO;;
        }
        
        //Save new field
        $isReplaced = $board->isFieldExists($x, $y);
        $replacedField = null;
        if ($isReplaced){
            $replacedField = $board->getField($x, $y);
            $isLastMeteor = $replacedField->getIsLastMeteor();
            $this->replaceFieldOnPlayerBoard($player_id, $route, $routeType, $rotation, $flipped, $x, $y, $isLastMeteor > 0 ? $isLastMeteor : 'null' );
            $field->setIsLastMeteor($isLastMeteor);
            $board->replaceField($x, $y, $field);
        } else {
            $this->addFieldToPlayerBoard($player_id, $route, $routeType, $rotation, $flipped, $x, $y);
            $board->addField($fieldRaw);
        }

        
        if ($dieId !== null){
            $this->useDie($dieId, $player_id);
        }
        
        if ($specialId !== null){
            $this->useSpecial($specialId, $player_id);
        }

        $this->saveMove($player_id, $field, $dieId, $specialId, $replacedField, $moveType); 

        $availableFields = $this->getAvailableFields($board);

        $usedDice[] = ["id" => $dieId];
        $canFinish = $this->canPlayerFinish($usedDice, $player_id);
       
        $this->notifyPlayer( $player_id, "newAvailableFields", "", [
            "availableFields" => $availableFields,
            "canFinish"=> $canFinish,
            "board" => $board->serialize($this->getGameStateValue(self::GAME_ROUND_GLOBAL), $this->getExpansionRouteType())
        ] );
    }

    function saveMove($playerId, $newField, $dieId, $specialId, $replacedField, $moveType) {
        $sql = "SELECT max(move_id) FROM move WHERE player_id = $playerId";
        $maxMove = $this->getUniqueValueFromDB($sql) ?? -1;
        $maxMove++;

        $moveValues = [
            $playerId, 
            $newField !== null ? $newField->getX() : "null", 
            $newField !== null ? $newField->getY() : "null", 
            $dieId ?? "null", 
            $specialId ?? "null", 
            $maxMove,
            "'$moveType'"
        ];

        $replacedValues = [
            'null',
            'null',
            'null',
            'null',
            'null',
            'null'
        ];

        if ($replacedField !== null){
            $replacedValues = [
                $replacedField->getRoute(),
                $replacedField->getRouteType(),
                $replacedField->isFlipped() ? 1 : 0,
                $replacedField->getRotation(),
                $replacedField->getRound() ?? "null",
                $replacedField->getIsLastMeteor() > 0 ? $replacedField->getIsLastMeteor() : "null",
            ];
        }

        $moveValues = array_merge($moveValues, $replacedValues);

        $values = implode(",", $moveValues);

        $sql = "INSERT INTO move (player_id, x, y, die_id, special_id, move_id, move_type, replaced_route, replaced_route_type, replaced_is_flipped, replaced_rotate, replaced_round, replaced_is_last_meteor) VALUES ($values)";
        $this->DbQuery($sql);
    }


// ███████╗████████╗ █████╗ ████████╗███████╗     █████╗ ██████╗  ██████╗ ██╗   ██╗███╗   ███╗███████╗███╗   ██╗████████╗███████╗
// ██╔════╝╚══██╔══╝██╔══██╗╚══██╔══╝██╔════╝    ██╔══██╗██╔══██╗██╔════╝ ██║   ██║████╗ ████║██╔════╝████╗  ██║╚══██╔══╝██╔════╝
// ███████╗   ██║   ███████║   ██║   █████╗      ███████║██████╔╝██║  ███╗██║   ██║██╔████╔██║█████╗  ██╔██╗ ██║   ██║   ███████╗
// ╚════██║   ██║   ██╔══██║   ██║   ██╔══╝      ██╔══██║██╔══██╗██║   ██║██║   ██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║   ╚════██║
// ███████║   ██║   ██║  ██║   ██║   ███████╗    ██║  ██║██║  ██║╚██████╔╝╚██████╔╝██║ ╚═╝ ██║███████╗██║ ╚████║   ██║   ███████║
// ╚══════╝   ╚═╝   ╚═╝  ╚═╝   ╚═╝   ╚══════╝    ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
      
    function argUseDice($playerId)
    {
        $board = $this->getPlayerBoards($playerId);
        $usedDice = $this->getDiceUsage();
        
        $privateData = [];

        $availableFields = $this->getAvailableFields($board);

        $specialRouteUsage = $this->getSpecialUsage();
        $usedSpecial = isset($specialRouteUsage[$playerId]) ? $specialRouteUsage[$playerId] : [];

        $specialRouteUsedThisTurn = isset($usedSpecial["currentRoundUsed"]) && $usedSpecial["currentRoundUsed"];
        $usedSpecialRoutesNumber = isset($usedSpecial["routes"]) ? sizeof($usedSpecial["routes"]) : 0;

        $args = [
            "availableFields" => $availableFields,
            "canFinish" => isset($usedDice[$playerId]) && $this->canPlayerFinish($usedDice[$playerId], $playerId),
            "specialRouteUsedThisTurn" => $specialRouteUsedThisTurn,
            "usedSpecialRoutesNumber" => $usedSpecialRoutesNumber,
            "isLavaExpansion" => $this->getExpansionRouteType() === self::EXPANSION_LAVA_OPTION_VALUE
        ];

        return $args;
    }
    
    function argDecideMeteor($playerId)
    {
        $board = $this->getPlayerBoards($playerId);
        $dice = $this->getCurrentDiceValues();
        
        $possibleTargets = $board->calculateMeteorTargets($dice[4]["route"], $dice[5]["route"]);

        $specialRouteUsage = $this->getSpecialUsage();
        $usedSpecial = isset($specialRouteUsage[$playerId]) ? $specialRouteUsage[$playerId] : [];

        $specialRouteUsedThisTurn = isset($usedSpecial["currentRoundUsed"]) && $usedSpecial["currentRoundUsed"];
        $usedSpecialRoutesNumber = isset($usedSpecial["routes"]) ? sizeof($usedSpecial["routes"]) : 0;

        $args = [
            "possibleTargets" => $possibleTargets,
            "specialRouteUsedThisTurn" => $specialRouteUsedThisTurn,
            "usedSpecialRoutesNumber" => $usedSpecialRoutesNumber,
        ];

        return $args;
    }

// ███████╗███╗   ██╗████████╗███████╗██████╗ ██╗███╗   ██╗ ██████╗     ███████╗████████╗ █████╗ ████████╗███████╗
// ██╔════╝████╗  ██║╚══██╔══╝██╔════╝██╔══██╗██║████╗  ██║██╔════╝     ██╔════╝╚══██╔══╝██╔══██╗╚══██╔══╝██╔════╝
// █████╗  ██╔██╗ ██║   ██║   █████╗  ██████╔╝██║██╔██╗ ██║██║  ███╗    ███████╗   ██║   ███████║   ██║   █████╗  
// ██╔══╝  ██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██║██║╚██╗██║██║   ██║    ╚════██║   ██║   ██╔══██║   ██║   ██╔══╝  
// ███████╗██║ ╚████║   ██║   ███████╗██║  ██║██║██║ ╚████║╚██████╔╝    ███████║   ██║   ██║  ██║   ██║   ███████╗
// ╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚══════╝   ╚═╝   ╚═╝  ╚═╝   ╚═╝   ╚══════╝
    
    function stPrepareRound()
    {
        $this->incGameStateValue(self::GAME_ROUND_GLOBAL, 1);
        
        foreach ($this->loadPlayersBasicInfos() as $playerId => $player) {
            $this->giveExtraTime($playerId);
        }

        $this->rollDiceAndPersist();
        $this->resetDiceUse();
        $this->resetMoves();
        $this->notifyAllPlayers( "newRound", clienttranslate("New round starts."), [
            "round" => $this->getGameStateValue(self::GAME_ROUND_GLOBAL),
            "dice" => $this->getCurrentDiceValues(),
            "usedDice" => []
        ]);

        if ($this->getExpansionRouteType() == self::EXPANSION_LAVA_OPTION_VALUE){
            $this->resetVolcanoUse();
        }

        $this->gamestate->nextState("playerTurn");
    }

    function stPlayerTurn() {
        $this->gamestate->setAllPlayersMultiactive();
        $this->gamestate->initializePrivateStateForAllActivePlayers();

        if ($this->getExpansionRouteType() == self::EXPANSION_METEOR_OPTION_VALUE){
            $dice = $this->getCurrentDiceValues();

            if ($dice[5]["route"] == 7){
                return $this->gamestate->nextPrivateStateForAllActivePlayers("decideMeteor");
            } else {
                return $this->gamestate->nextPrivateStateForAllActivePlayers("decideMeteorCancel");
            }
        }
    }
    
    function stCheckGameEnd() {
        $round = $this->getGameStateValue(self::GAME_ROUND_GLOBAL);
        $boards = $this->getPlayerBoards();

        $this->notifyAllPlayers("spectatorUpdate", "", [
            "boards" => array_map(function ($board) use ($round) { 
                return $board->filterLastRoundAndSerialize($round); 
            }, $boards)
        ]);
        
        if ($round >= $this->getTotalNumberOfRounds()) {
            foreach ($boards as $playerId => $board) {
                $score = $board->getScore($this->getExpansionRouteType());
                $this->dbSetScore($playerId, $score["total"], $score["errors"]);

                $specialRouteUsage = $this->getSpecialUsage();
                $usedSpecial = isset($specialRouteUsage[$playerId]) ? $specialRouteUsage[$playerId] : [ "routes" => []];

                $lastSpecialRouteRound = array_reduce($usedSpecial["routes"], function($carry, $route){
                    if ($route["round"] > $carry){
                        return $route["round"];
                    }

                    return $carry;
                }, 0); 
                $groupSizeStat = array_reduce($score["exits"]["exitGroups"], function($carry, $group){
                    $size = sizeof($group["exits"]);

                    if ($size > 1){
                        $carry[$size]++;
                        $carry["total"]++;
                    }

                    return $carry;
                }, [
                    "total" => 0,
                    2 => 0, 
                    3 => 0, 
                    4 => 0, 
                    5 => 0, 
                    6 => 0, 
                    7 => 0, 
                    8 => 0, 
                    9 => 0, 
                    10 => 0, 
                    11 => 0, 
                    12 => 0, 
                ]);

                $this->setStat($score["total"], RRIStats::TOTAL_POINTS, $playerId);
                $this->setStat($score["exits"]["total"], RRIStats::POINTS_FROM_EXITS, $playerId);
                $this->setStat($score["longestHighway"]["total"], RRIStats::POINTS_FROM_LONGEST_HIGHWAY, $playerId);
                $this->setStat($score["longestRailroad"]["total"], RRIStats::POINTS_FROM_LONGEST_RAILROAD, $playerId);
                $this->setStat($score["centralFields"]["total"], RRIStats::POINTS_FROM_CENTER, $playerId);
                if ($this->isExpansionIncluded()){
                    $this->setStat($score["special"]["total"], RRIStats::POINTS_FROM_EXPANSION, $playerId);
                }
                $this->setStat($score["errors"]["total"], RRIStats::LOST_POINTS_ERRORS, $playerId);
                $this->setStat(sizeof($usedSpecial["routes"]), RRIStats::USED_SPECIAL_ROUTES, $playerId);
                $this->setStat($lastSpecialRouteRound, RRIStats::SPECIAL_ROUTE_LAST_ROUND, $playerId);
                $this->setStat($groupSizeStat["total"], RRIStats::TOTAL_EXIT_GROUPS, $playerId);
                $this->setStat($groupSizeStat[2], RRIStats::TWO_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[3], RRIStats::THREE_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[4], RRIStats::FOUR_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[5], RRIStats::FIVE_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[6], RRIStats::SIX_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[7], RRIStats::SEVEN_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[8], RRIStats::EIGHT_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[9], RRIStats::NINE_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[10], RRIStats::TEN_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[11], RRIStats::ELEVEN_EXITS_CONNECTED, $playerId);
                $this->setStat($groupSizeStat[12], RRIStats::TWELVE_EXITS_CONNECTED, $playerId);
            }

            $this->gamestate->nextState( "end" );
            return;
        }

        $this->gamestate->nextState( "nextRound" );
    }

// ███████╗ ██████╗ ███╗   ███╗██████╗ ██╗███████╗
// ╚══███╔╝██╔═══██╗████╗ ████║██╔══██╗██║██╔════╝
//   ███╔╝ ██║   ██║██╔████╔██║██████╔╝██║█████╗  
//  ███╔╝  ██║   ██║██║╚██╔╝██║██╔══██╗██║██╔══╝  
// ███████╗╚██████╔╝██║ ╚═╝ ██║██████╔╝██║███████╗
// ╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚═════╝ ╚═╝╚══════╝

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */

    function zombieTurn($state, $active_player)
    {
        $statename = $state['name'];

        if ($state['type'] === "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState("zombiePass");
                    break;
            }

            return;
        }

        if ($state['type'] === "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $this->gamestate->setPlayerNonMultiactive($active_player, '');

            return;
        }

        throw new feException("Zombie mode not supported at this game state: " . $statename);
    }

// ██████╗ ██████╗     ██╗   ██╗██████╗  ██████╗ ██████╗  █████╗ ██████╗ ███████╗
// ██╔══██╗██╔══██╗    ██║   ██║██╔══██╗██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██╔════╝
// ██║  ██║██████╔╝    ██║   ██║██████╔╝██║  ███╗██████╔╝███████║██║  ██║█████╗  
// ██║  ██║██╔══██╗    ██║   ██║██╔═══╝ ██║   ██║██╔══██╗██╔══██║██║  ██║██╔══╝  
// ██████╔╝██████╔╝    ╚██████╔╝██║     ╚██████╔╝██║  ██║██║  ██║██████╔╝███████╗
// ╚═════╝ ╚═════╝      ╚═════╝ ╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */

    function upgradeTableDb($from_version)
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345

        if ($from_version <= 2106041616){
            try {
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_is_last_meteor` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `move_type` varchar(16) NOT NULL";  
                $this->applyDbUpgradeToAllDB( $sql );
                
                $sql = "ALTER TABLE `DBPREFIX_move` MODIFY `x` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                
                $sql = "ALTER TABLE `DBPREFIX_move` MODIFY `y` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );

                $sql = "UPDATE `DBPREFIX_player` SET `player_state` = 12";
                $this->applyDbUpgradeToAllDB( $sql );

                $sql = "SELECT global_value FROM global WHERE global_id = 1";
                $state = $this->getUniqueValueFromDB($sql);

                if ($state == 11 || $state == 10) {
                    $sql = "UPDATE `DBPREFIX_player` SET `player_state` = $state WHERE player_is_multiactive = 1";
                    $this->applyDbUpgradeToAllDB( $sql );
                    
                    $sql = "UPDATE `DBPREFIX_player` SET `player_is_multiactive` = 1 WHERE player_is_multiactive = 0";
                    $this->applyDbUpgradeToAllDB( $sql );
                }
                
                $sql = "UPDATE `DBPREFIX_global` SET `global_value` = 2 WHERE `global_id` = 1";
                $this->applyDbUpgradeToAllDB( $sql );
            } catch (Exception $ex) {
                
            }
        }
        
        if( $from_version <= 2103021437 )
        {
            $sql = <<<SQL
                CREATE TABLE IF NOT EXISTS `DBPREFIX_moves` (
                    `player_id` INT(10) NOT NULL,
                    `x` INT(1) NOT NULL,
                    `y` INT(1) NOT NULL,
                    `die_id` INT(1) NULL,
                    `special_id` INT(1) NULL,
                    `move_id` INT(1) NOT NULL
                )
SQL;
            $this->applyDbUpgradeToAllDB( $sql );
        }

        if ($from_version <= 2103221843){
            $sql = "ALTER TABLE `DBPREFIX_board` ADD `is_last_meteor` INT(1) NULL";

            try {
                $this->applyDbUpgradeToAllDB( $sql );
            } catch (Exception $ex) {

            }
        }
        
        if ($from_version <= 2103241051){
            try {
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `is_replaced_meteor` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_meteor_round` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
            } catch (Exception $ex) {

            }
        }
        
        if ($from_version <= 2103241255){
            try {
                $sql = "ALTER TABLE `DBPREFIX_player` ADD `used_volcano` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_route` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_route_type` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_is_flipped` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_rotate` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
                $sql = "ALTER TABLE `DBPREFIX_move` ADD `replaced_round` INT(1) NULL";
                $this->applyDbUpgradeToAllDB( $sql );
            } catch (Exception $ex) {

            }
        }

    }
}
