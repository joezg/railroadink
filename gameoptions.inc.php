<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * gameoptions.inc.php
 *
 * railroadink game options description
 * 
 * In this file, you can define your game options (= game variants).
 *   
 * Note: If your game has no variant, you don't have to modify this file.
 *
 * Note²: All options defined in this file should have a corresponding "game state labels"
 *        with the same ID (see "initGameStateLabels" in railroadink.game.php)
 *
 * !! It is not a good idea to modify this file when a game is running !!
 *
 */

$game_options = array(
    101 => [
        "name" => totranslate("Edition"),
        "values" => [
            1 => [
                "name" => totranslate("Deep Blue"),
                "tmdisplay" => totranslate("Deep Blue")
            ],
            2 => [
                "name" => totranslate("Blazing Red"),
                "tmdisplay" => totranslate("Blazing Red")
            ]
        ]
    ],
    100 => [
        "name" => totranslate("Expansion"),
        "values" => [
            1 => [
                "name" => totranslate("Base game only")
            ],
            2 => [
                "name" => totranslate("River"),
                "tmdisplay" => totranslate("River")
            ],
            3 => [
                "name" => totranslate("Lake"),
                "tmdisplay" => totranslate("Lake")

            ],
        ],
        'displaycondition' => [ 
            [
                'type' => 'otheroption',
                'id' => 101,
                'value' => [1]
            ],
        ],
        "default" => 1
    ],
    102 => [
        "name" => totranslate("Expansion"),
        "values" => [
            1 => [
                "name" => totranslate("Base game only")
            ],
            4 => [
                "name" => totranslate("Lava"),
                "tmdisplay" => totranslate("Lava")
            ],
            5 => [
                "name" => totranslate("Meteor"),
                "tmdisplay" => totranslate("Meteor")
            ],
        ],
        'displaycondition' => [ 
            [
                'type' => 'otheroption',
                'id' => 101,
                'value' => [2]
            ],
        ],
        "default" => 1
    ]
);


