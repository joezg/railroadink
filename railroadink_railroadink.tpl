{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- railroadink implementation : © <Your name here> <Your email address here>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    railroadink_railroadink.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
    
    Please REMOVE this comment before publishing your game on BGA
-->

<div class="banner"></div>

<div id="all-players" class="player-count-{PLAYER_COUNT} edition-{EDITION}">
    <div id="active-player">
        <div id="infrastructure">
            <div id="round_counter">
                Round: <span id="round-display"><span id="round">0</span> / <span id="total-rounds">?</span></span>
            </div>
            <div id="dice">
                <!-- BEGIN die -->
                <div id="die-{ID}" class="die" data-id="{ID}"></div>
                <!-- END die -->
            </div>
        </div>
    </div>


    <div class="player-sheets">
        <!-- BEGIN player -->
        <div id="player-area-{PLAYER_ID}" class="player-area">
            <h2 style="color:#{PLAYER_COLOR}">{NAME}</h2>
            <div class="player-sheet-wrapper">
                <div class="player-sheet-wrapper-conteiner">
                    <div id="player-sheet-{PLAYER_ID}" class="player-sheet">
                        <div class="special-routes">
                            <!-- BEGIN special_route -->
                            <div class="special-route special-route-{ROUTE_ID}" data-route="{ROUTE_ID}">
                                <div class="special-cross"></div>
                            </div>
                            <!-- END special_route -->
                        </div>
                        <div class="score">
                            <div class="score-part exits" data-score-highlight="score-highlight-exits"></div>
                            <div class="score-part highway" data-score-highlight="score-highlight-highway"></div>
                            <div class="score-part railroad" data-score-highlight="score-highlight-railroad"></div>
                            <div class="score-part central" data-score-highlight="score-highlight-central"></div>
                            <div class="score-part errors" data-score-highlight="score-highlight-errors"></div>
                            <div class="score-part special" data-score-highlight="score-highlight-special"></div>
                            <div class="score-part total" data-score-highlight="score-highlight-total"></div>
                        </div>
                        <!-- BEGIN field -->
                        <div class="field field-{ROW}-{COL}" data-row="{ROW}" data-col="{COL}" style="top: {TOP}%; left: {LEFT}%">
                            <div class="route"></div>
                            <div class="interactions">
                                <a href="#" class="bgabutton bgabutton_blue left">&#x293f;</a>
                                <a href="#" class="bgabutton bgabutton_blue right">&#x293e;</a>
                                <a href="#" class="bgabutton bgabutton_blue flip">&#x293a;</a>
                                <a href="#" class="bgabutton bgabutton_blue done">&#x2713;</a>
                            </div>
                            <div class="mistakes">
                                <div class="mistake mistake-n"></div>
                                <div class="mistake mistake-e"></div>
                                <div class="mistake mistake-w"></div>
                                <div class="mistake mistake-s"></div>
                            </div>
                            <div class="last-meteor"></div>
                            <div class="round-number"></div>
                        </div>
                        <!-- END field -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END player -->
    </div>

</div>

<script type="text/javascript">

// Javascript HTML templates

var jstpl_dice_tooltip='<div class="dice_tooltip">${dice}</div></div>';
var jstpl_die='<div class="die route-${route} die-type-${type}"></div>';
/*
// Example:

*/

</script>  

{OVERALL_GAME_FOOTER}
