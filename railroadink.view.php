<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * railroadink.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in railroadink_railroadink.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */

require_once(APP_BASE_PATH . "view/common/game.view.php");
require_once('modules/RRIField.php');

class view_railroadink_railroadink extends game_view
{
  function getGameName()
  {
    return "railroadink";
  }
  function build_page($viewArgs)
  {
    // Get players & players number
    $players = $this->game->loadPlayersBasicInfos();
    $players_nbr = count($players);

    /*********** Place your code below:  ************/
    $this->tpl["PLAYER_COUNT"] = $players_nbr;
    $this->tpl["EDITION"] = $this->game->getEdition();

    $this->page->begin_block("railroadink_railroadink", "special_route");
    $this->page->begin_block("railroadink_railroadink", "field");
    $this->page->begin_block("railroadink_railroadink", "player");

    foreach ($players as $playerId => $player) {
      $this->page->reset_subblocks("special_route");
      foreach (RRIField::getSpecialRoutes() as $routeId => $route) {
        $this->page->insert_block("special_route", ["ROUTE_ID" => $routeId]);
      }
      $this->page->reset_subblocks('field');
      for ($row = 0; $row < 7; $row++) {
        for ($col = 0; $col < 7; $col++) {
          $this->page->insert_block("field", array(
            "ROW" => $row,
            "COL" => $col,
            "LEFT" => $row * 12.857 + 4.857,
            "TOP" => $col * 8.57 + 36.57,
          ));
        }
      }
      $this->page->insert_block('player', array(
        "PLAYER_ID" => $playerId,
        "NAME" => $player["player_name"],
        "PLAYER_COLOR" => $player["player_color"]
      ));
    }

    $this->page->begin_block("railroadink_railroadink", "die");
    $dice = $this->game->getCurrentDiceValues();
    for ($id = 0; $id < sizeof($dice); $id++) {
      $this->page->insert_block('die', ["ID" => $id]);
    }
    /*********** Do not change anything below this line  ************/
  }
}
