/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * railroadink.js
 *
 * railroadink user interface script
 *
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

 var ROUTE_TYPE_REGULAR = 0;
 var ROUTE_TYPE_SPECIAL = 1;
 var ROUTE_TYPE_LAVA = 4;

define([
  'dojo',
  'dojo/_base/declare',
  'dojo/NodeList-traverse',
  'ebg/core/gamegui',
  'ebg/counter'
], function(dojo, declare) {
  //TODO add sounds

  var eventHandles = {
    handles: {},
    attach: function(node, group, event, handle) {
      if (!this.handles[group]) {
        this.handles[group] = [];
      }

      this.handles[group].push(dojo.connect(node, event, handle));
    },
    attachQuery: function(query, group, event, handle) {
      query.forEach(
        function(node) {
          this.attach(node, group, event, handle);
        }.bind(this)
      );
    },
    detachAll: function(group) {
      if (!this.handles[group]) {
        return;
      }

      this.handles[group].forEach(function(handle) {
        handle.remove();
      });
      this.handles[group] = [];
    }
  };

  var dice = {
    value: [],
    selectedDie: null,
    element: dojo.query('#dice'),
    set: function(dice, usedDice) {
      this.value = dice;
      dice.forEach(this.setDie.bind(this));
      usedDice.forEach(this.useDie.bind(this));
    },
    reset: function() {
      this.unselectAll();
      this.unuseAll();
      this.removeRoutes();
    },
    setDie: function(die) {
      dojo.addClass('die-' + die.id, 'route-' + die.route);
      dojo.addClass('die-' + die.id, 'die-type-' + die.routeType);
    },
    useDie: function(die) {
      dojo.addClass('die-' + die.id, 'used');
      dojo.removeClass('die-' + die.id, 'selected');
    },
    unuseAll: function(die) {
      this.element.query('.die').removeClass('used');
      this.element.query('.die').removeClass('selected');
    },
    unselectAll: function() {
      this.element.query('.die').removeClass('selected');
    },
    removeRoutes: function() {
      var diceElements = this.element.query('.die');
      diceElements.removeClass();
      diceElements.addClass("die");
    },
    activate: function(onSelected) {
      this.element.addClass('active');
      this.selectedDie = null;

      eventHandles.attachQuery(
        dojo.query('#dice .die'),
        'diceActivation',
        'click',
        function(e) {
          var dieElement = dojo.query(e.target);

          this.unselectAll();
          dieElement.addClass('selected');

          var selectedDieId = +dojo.attr(e.target, 'data-id');
          this.selectedDie = this.value.find(function(die) {
            return +die.id === +selectedDieId;
          });
          onSelected(this.selectedDie);
        }.bind(this)
      );
    },
    deactivate: function() {
      this.element.removeClass('active');
      this.unselectAll();
      eventHandles.detachAll('diceActivation');
    }
  };

  var boards = {
    set: function(players) {
      Object.values(players).forEach(this.setBoard.bind(this));
    },
    setBoard: function(player) {
        var playerBoard = this.getPlayerBoard(player.id);
        playerBoard.setFields(player.board.fields);
        playerBoard.setScore(player.board.score);
        if (player.isIdle){
          playerBoard.setIdle()
        }
    },
    setAllBoardsActive: function(){
      dojo.query(".player-sheet-wrapper.idle").removeClass("idle");
    },
    getPlayerBoard: function(player) {
      var playerBoardElement = dojo.query('#player-sheet-' + player);
      return {
        setFields: function(fields) {
          fields.forEach(this.setField.bind(this));
        },
        setField: function(field) {
          this.getField(field.x, field.y).set(field);
        },
        setIdle: function(){
          playerBoardElement.closest(".player-sheet-wrapper").addClass("idle");
        },
        setActive: function(){
          playerBoardElement.closest(".player-sheet-wrapper").removeClass("idle");
        },
        resetFields: function(fields){
          this.deactivateAllFields();
          this.removeAllMistakes();
          this.getAllFields().reset();
          if (fields){
            this.setFields(fields);
          }
        },
        resetSpecificFields: function(fields){
          this.deactivateAllFields();
          this.removeAllMistakes();
          this.getSpecificFields(fields).reset();
          playerBoardElement.query(".field .current-route").removeClass("current-route");
          this.setFields(fields);
        },
        removeAllMistakes: function(){
          playerBoardElement.query('.mistake.active').removeClass("active");
        },
        removeMeteorTargets: function(){
          playerBoardElement.query('.field.target').removeClass("target");
        },
        getField: function(x, y) {
          var fieldElement = playerBoardElement.query('.field-' + x + '-' + y);
          var routeElement = fieldElement.query(' .route');
          var roundElement = fieldElement.query(' .round-number');
          var lastMeteorElement = fieldElement.query(' .last-meteor');
          return {
            isSet: function(){
              return routeElement.attr("data-route")[0] !== null;
            },
            isLastMeteor: function() {
              return lastMeteorElement[0].classList.contains("active")
            },
            getRound(){
              const innerHTML = roundElement.innerHTML(); 
              return innerHTML.length > 0 ? +innerHTML : innerHTML;
            },
            set: function(field) {
              routeElement.addClass('set');

              routeElement.attr("data-route", field.route);

              routeElement.addClass('route-' + field.route);
              routeElement.addClass('route-type-' + field.routeType);
              routeElement.attr("data-route-type", field.routeType);

              var rotation = field.rotation || 0;
              routeElement.attr('data-rotation', rotation);

              routeElement.attr('data-flipped', +field.flipped);

              this.addTransform();

              if (!field.round || field.isCurrent){
                this.setCurrent();
              }

              if (field.round){
                roundElement.innerHTML(field.round);
              }

              if (field.isLastMeteor) {
                lastMeteorElement.addClass("active");
              }

              if (field.isReplaced){
                routeElement.attr("data-replaced", 1);
                routeElement.attr("data-replaced-route", field.replacedData.route);
                routeElement.attr("data-replaced-route-type", field.replacedData.routeType);
                routeElement.attr("data-replaced-flipped", field.replacedData.flipped);
                routeElement.attr("data-replaced-rotation", field.replacedData.rotation);
                routeElement.attr("data-replaced-round", field.replacedData.round);
              }
            },
            getData: function() {
              return {
                x: x,
                y: y,
                route: routeElement.attr('data-route')[0],
                routeType: routeElement.attr('data-route-type')[0],
                flipped: routeElement.attr('data-flipped')[0],
                rotation: routeElement.attr('data-rotation')[0],
              }
            },
            setCurrent: function(){
              routeElement.addClass("current-route");
            },
            addTransform() {
              var rotation = +routeElement.attr('data-rotation'),
                flipped = +routeElement.attr('data-flipped'),
                degrees = rotation * 90,
                scale = !!flipped ? -1 : 1,
                transformValue =
                  'rotate(' + degrees + 'deg) scaleX(' + scale + ')';

              routeElement.style({
                transform: transformValue
              });
            },
            flip: function(possibilities) {
              var next = +!+routeElement.attr('data-flipped');
              routeElement.attr('data-flipped', next);
              if (possibilities) {
                this.rotate(1, possibilities);
              }

              this.addTransform();
            },
            getNormalizedRotation(rotation) {
              var normalized = rotation % 4;

              return normalized < 0 ? normalized + 4 : normalized;
            },
            rotate: function(direction, possibilities) {
              var current = +routeElement.attr('data-rotation'),
                next = current + direction;

              if (possibilities) {
                var isFlipped = !!+routeElement.attr('data-flipped');
                var rotations = isFlipped
                  ? possibilities.flippedRotations
                  : possibilities.normalRotations;

                var counter = 0;
                while (
                  counter < 4 &&
                  rotations.indexOf(this.getNormalizedRotation(next)) === -1
                ) {
                  next = next + direction;
                  counter++;
                }
              }

              routeElement.attr('data-rotation', next);
              this.addTransform();
            },
            makeInteractive: function(selectedRoute, possibilities, onDone) {
              fieldElement.addClass('interactive');
              eventHandles.attachQuery(
                fieldElement.query('.interactions .left'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  this.rotate(-1, possibilities);
                }.bind(this)
              );
              eventHandles.attachQuery(
                fieldElement.query('.interactions .right'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  this.rotate(1, possibilities);
                }.bind(this)
              );
              if (
                possibilities.flippedRotations.length === 0 ||
                possibilities.normalRotations.length === 0
              ) {
                fieldElement
                  .query('.interactions .flip')
                  .style('display', 'none');
              } else {
                fieldElement
                  .query('.interactions .flip')
                  .style('display', 'inherit');
                eventHandles.attachQuery(
                  fieldElement.query('.interactions .flip'),
                  'fieldInteractions',
                  'click',
                  function(e) {
                    dojo.stopEvent(e);
                    this.flip(possibilities);
                  }.bind(this)
                );
              }
              eventHandles.attachQuery(
                fieldElement.query('.interactions .done'),
                'fieldInteractions',
                'click',
                function(e) {
                  dojo.stopEvent(e);
                  onDone(this, selectedRoute);
                }.bind(this)
              );
            },
            turnOffInteractive: function() {
              fieldElement.removeClass('interactive');
            },
            setRouteClass: function(className){
              routeElement.addClass(className);
            },
            addMistake: function(direction){
              fieldElement.query(`.mistake.mistake-${direction}`).addClass("active");
            }
          };
        },
        getAllFields: function() {
          return this.getFields();
        },
        getSpecificFields: function(coords) {
          if (coords.length === 0) {
            return this.getFields(false);
          }
          var selectors = coords.map(function(c) {
            return '.field-' + c.x + '-' + c.y;
          });

          return this.getFields(selectors.join(','));
        },
        getFields: function(query) {
          var fieldElements =
            query === false
              ? new dojo.NodeList()
              : playerBoardElement.query(query || '.field');

          var routeElements = fieldElements.query('.route');
          var playerBoard = this;

          return {
            getFieldElements(){
              return fieldElements;
            },
            activateCustom: function(onFieldSelected, additionalClass = ""){
              fieldElements.addClass('available');
              fieldElements.addClass(additionalClass);

              eventHandles.attachQuery(
                fieldElements,
                'placeDieOnField',
                'click',
                function(e) {
                  var selectedField = dojo.query(e.target).closest('.field'),
                    row = selectedField.attr('data-row')[0],
                    col = selectedField.attr('data-col')[0];

                  onFieldSelected(row, col);
                }
              );
            },
            activate: function(selectedDie, onFieldSelected) {
              this.activateCustom((row, col) => {
                if (selectedDie !== null) {
                  onFieldSelected(
                    playerBoard.getField(row, col),
                    selectedDie
                  );
                }
              });
            },
            deactivate: function() {
              fieldElements.removeClass('available');
              eventHandles.detachAll('placeDieOnField');
            },
            reset: function() {
              fieldElements.removeClass('interactive');
              [
                'set',
                'route-0',
                'route-1',
                'route-2',
                'route-3',
                'route-4',
                'route-5',
                'route-6',
                'route-7',
                'route-8',
                'route-type-0',
                'route-type-1',
                'route-type-2',
                'route-type-3',
                'route-type-4',
                'route-type-5',
                'current-route',
                'longest-highway',
                'longest-railroad',
                "score-mistake",
                "special",
                'central',
                'exits-0',
                'exits-1',
                'exits-2',
                'exits-3',
                'exits-4',
                'exits-5',
              ].forEach(routeElements.removeClass.bind(routeElements));
              routeElements.style({ transform: 'none' });
              routeElements.removeAttr("data-route")
              routeElements.removeAttr("data-route-type")
              routeElements.removeAttr("data-rotation")
              routeElements.removeAttr("data-flipped")
              
              var roundElements = fieldElements.query('.round-number');
              roundElements.innerHTML("");

              var lastMeteorElements = fieldElements.query('.last-meteor');
              lastMeteorElements.removeClass("active");
              routeElements.removeAttr("data-replaced");
              routeElements.removeAttr("data-replaced-route");
              routeElements.removeAttr("data-replaced-route-type");
              routeElements.removeAttr("data-replaced-flipped")
              routeElements.removeAttr("data-replaced-rotation");              
              routeElements.removeAttr("data-replaced-round")              
              
              eventHandles.detachAll('fieldInteractions');
            },
            removeCurrentRoundHighlights: function(){
              routeElements.removeClass('current-route');
            }
          };
        },
        activateAvailableFields: function(
          availableFields,
          selectedDie,
          onFieldSelected
        ) {
          this.getAllFields().deactivate();

          var fields = this.getSpecificFields(availableFields);
          fields.activate(selectedDie, onFieldSelected);
        },
        deactivateAllFields: function() {
          this.getAllFields().deactivate();
        },
        activateSpecialRoutes: function(onSelectedSpecialRoute) {
          var specialRoutes = playerBoardElement.query('.special-routes');
          specialRoutes.addClass('active');
          eventHandles.attachQuery(
            playerBoardElement.query('.special-route'),
            'specialRoutesUse',
            'click',
            function(e) {
              var route = dojo.attr(e.target, 'data-route');

              specialRoutes.query('.special-route').removeClass('selected');
              var selectedSpecialRoute = dojo.query(e.target);
              selectedSpecialRoute.addClass('selected');

              onSelectedSpecialRoute({ route: route, routeType: ROUTE_TYPE_SPECIAL });
            }
          );
        },
        deactivateSpecialRoutes: function() {
          var specialRoutes = playerBoardElement.query('.special-routes');
          specialRoutes.removeClass('active');
          this.unselectAllSpecialRoutes();
          eventHandles.detachAll('specialRoutesUse');
        },
        useSpecialRoute: function(specialRoute) {
          var specialRoute = playerBoardElement.query(
            '.special-routes .special-route-' + specialRoute.route
          );
          specialRoute.addClass('used');
          specialRoute.removeClass('selected');
        },
        unselectAllSpecialRoutes: function() {
          playerBoardElement.query('.special-route').removeClass('selected');
        },
        unuseAllSpecialRoutes: function() {
          playerBoardElement.query('.special-route').removeClass('used');
        },
        clearInteractiveField: function() {
          const interactiveFields = this.getFields('.field.interactive');
          const routeElements = interactiveFields.getFieldElements().query('.route');

          if (routeElements.length > 0){
            if (routeElements.attr("data-replaced")[0] === "1"){
              const x = interactiveFields.getFieldElements().attr("data-row")[0];
              const y = interactiveFields.getFieldElements().attr("data-col")[0];
              const field = this.getField(x, y); 
              const isLastMeteor = field.isLastMeteor();
              
              const replacedField = {
                route: routeElements.attr("data-replaced-route")[0],
                routeType: routeElements.attr("data-replaced-route-type")[0],
                rotation: routeElements.attr("data-replaced-rotation")[0],
                flipped: routeElements.attr("data-replaced-flipped")[0],
                isLastMeteor: isLastMeteor,
                round: routeElements.attr("data-replaced-round")[0]
              }; 

              interactiveFields.reset();
              field.set(replacedField);

              return;
            }
          }
          
          interactiveFields.reset();
        },
        setScore: function(score) {
          var scoreElement = playerBoardElement.query('.score');

          scoreElement.query(".score-part.total").innerHTML(score.total);
          scoreElement.query(".score-part.exits").innerHTML(score.exits.total);
          scoreElement.query(".score-part.highway").innerHTML(score.longestHighway.total);
          scoreElement.query(".score-part.railroad").innerHTML(score.longestRailroad.total);
          scoreElement.query(".score-part.central").innerHTML(score.centralFields.total);
          scoreElement.query(".score-part.errors").innerHTML(score.errors.total);
          score.special && scoreElement.query(".score-part.special").innerHTML(score.special.total);

          this.scoreHighlightField(score.longestHighway.fields, "longest-highway");
          this.scoreHighlightField(score.longestRailroad.fields, "longest-railroad");
          this.scoreHighlightField(score.centralFields.fields, "central");
          score.special && this.scoreHighlightField(score.special.fields, "special");
          this.scoreHighlightExits(score.exits);
          this.scoreHighlightMistakes(score.errors.allErrorDirections);
        },
        scoreHighlightMistakes: function(mistakes){
          mistakes.forEach(mistake => {
            const field = this.getField(mistake.field.x, mistake.field.y);
            field.addMistake(mistake.direction.toLowerCase());
            field.setRouteClass("score-mistake");
          });
        },
        scoreHighlightField: function(fields, className){
          fields.forEach(field => {
            this.getField(field.x, field.y).setRouteClass(className);
          });
        },
        scoreHighlightExits: function(exits){
          const positiveExitGroups = exits.exitGroups.filter(g => g.score > 0);

          for (let i = 0; i < positiveExitGroups.length; i++) {
            const exitGroup = positiveExitGroups[i];
            this.scoreHighlightField(exitGroup.fields, `exits-${i}`)
          }
        }
      };
    }
  };

  return declare('bgagame.railroadink', ebg.core.gamegui, {
    constructor: function() {
      console.log('railroadink constructor');
      
      // Here, you can init the global variables of your user interface
      // Example:
      // this.myGlobalValue = 0;
    },

    /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedata" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */

    setup: function(gamedata) {
      console.log('Starting game setup');
      console.log(gamedata);

      this.addTooltips(gamedata.expansion);

      let mainPlayerId = this.player_id;
      if (this.isSpectator){
          mainPlayerId = Object.keys(gamedata.players)[0];
      } 
      dojo.place(`player-area-${mainPlayerId}`, "active-player", "first");

      eventHandles.attachQuery(dojo.query(".score-part"), "score-highlight", "mouseenter", e => {
        const scoreHighlightClass = dojo.attr(e.target, 'data-score-highlight');
        const playerSheet = dojo.query(e.target).closest(".player-sheet");
        playerSheet.addClass(scoreHighlightClass);
        playerSheet.addClass("score-highlight");
      })
      
      eventHandles.attachQuery(dojo.query(".score-part"), "score-highlight", "mouseleave", e => {
        const scoreHighlightClass = dojo.attr(e.target, 'data-score-highlight');
        const playerSheet = dojo.query(e.target).closest(".player-sheet");
        playerSheet.removeClass(scoreHighlightClass);
        playerSheet.removeClass("score-highlight");
      })

      this.setRound(gamedata.round, gamedata.totalRounds);

      // Setting up player boards
      for (var player_id in gamedata.players) {
        var player = gamedata.players[player_id];

        player.isIdle = player.is_multiactive == "0" && gamedata.gamestate.name == "useDice";

        if (player_id == this.player_id){
          if (player.used_volcano == 1){
            this.playerUsedVolcano = true;
          }
        }
      }
      var usedDice = gamedata.usedDice[this.player_id] || {};
      
      this.dice.set(gamedata.dice, Object.values(usedDice));
      this.boards.set(gamedata.players);

      for (const playerId in gamedata.usedSpecial) {
        if (Object.hasOwnProperty.call(gamedata.usedSpecial, playerId)) {
          const special = gamedata.usedSpecial[playerId];
          const board = this.boards.getPlayerBoard(playerId);

          Object.values(special.routes).forEach(function(special){
            board.useSpecialRoute({ route: special.id });
          });
        }
      }
      
      var usedSpecial = gamedata.usedSpecial[this.player_id];
      var currentPlayerBoard = this.boards.getPlayerBoard(this.player_id);
      this.setSpecialRoutesUseForPlayerBoard(usedSpecial, currentPlayerBoard);

      // Setup game notifications to handle (see "setupNotifications" method below)
      this.setupNotifications();

      if (gamedata.edition == 2){
        const root = document.getElementsByTagName( 'html' )[0];
        root.classList.add('red-edition');
      }

      console.log('Ending game setup');
    },

    ///////////////////////////////////////////////////
    //// Game & client states

    // onEnteringState: this method is called each time we are entering into a new game state.
    //                  You can use this method to perform some user interface changes at this moment.
    //
    onEnteringState: function(stateName, msg) {
      console.log('Entering state: ' + stateName);
      switch (stateName) {
        case "useDice":
          this.availableFields = Object.values(
            msg.args.availableFields
          );

          this.specialRouteUsedThisTurn = msg.args.specialRouteUsedThisTurn;
          this.usedSpecialRoutesNumber = msg.args.usedSpecialRoutesNumber;
          this.canFinish = msg.args.canFinish;
          this.checkDoneButton();
       
          this.setupDiceUse();
          break;
        case 'dummmy':
          break;
      }
    },

    // onLeavingState: this method is called each time we are leaving a game state.
    //                 You can use this method to perform some user interface changes at this moment.
    //
    onLeavingState: function(stateName) {
      console.log('Leaving state: ' + stateName);
      switch (stateName) {
        case 'useDice':
          this.availableFields = null;
          this.playerUsedVolcano = false;
          this.canFinish = false;
          break;
        case "decideMeteor":
        case "decideMeteorCancel":
          this.boards.getPlayerBoard(this.player_id).removeMeteorTargets();
        case 'dummmy':
          break;
      }
    },

    // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
    //                        action status bar (ie: the HTML links in the status bar).
    //
    onUpdateActionButtons: function(stateName, msg) {
      console.log('onUpdateActionButtons: ' + stateName);
      if (this.isCurrentPlayerActive()) {
        switch (stateName) {
          case 'useDice':
            this.addActionButton( 'custom_undo_button', _('undo'), 'onUndo' );
            this.addActionButton( 'custom_restart_button', _('restart'), 'onRestart' );
            this.addActionButton( 'done_button', _('done'), 'onDone' );

            if (msg.isLavaExpansion){
              const label = '<span class="add-volcano-label-image"></span>' + _('add volcano');
              this.addActionButton( 'add_volcano', label, 'onAddVolcano' );
              if (this.playerUsedVolcano){
                this.disableVolcanoButton();
              }
            }
            break;
          case "decideMeteor":
          case "decideMeteorCancel":
            this.boards.getPlayerBoard(this.player_id).deactivateSpecialRoutes();
            this.dice.deactivate();

            this.specialRouteUsedThisTurn = msg.specialRouteUsedThisTurn;
            this.usedSpecialRoutesNumber = msg.usedSpecialRoutesNumber

            if (this.isCurrentPlayerActive()){
              this.boards.getPlayerBoard(this.player_id)
                .getSpecificFields(msg.possibleTargets)
                .activateCustom(this.onDecideMeteor.bind(this), "target");

                if (this.canUseSpecialRoutes()){
                  this.boards.getPlayerBoard(this.player_id)
                    .activateSpecialRoutes(this.onDecidePrevent.bind(this));
                }

                if (stateName == "decideMeteorCancel"){
                  this.addActionButton( 'allowMeteor', _('Allow meteor strike'), 'onAllowMeteor' );

                }
            } 
            break;
          case 'dummmy':
            break;
        }
      }
    },

    ///////////////////////////////////////////////////
    //// Utility methods

    /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */
    dice: dice,
    boards: boards,
    addTooltips: function(expansion){
      this.addTooltipToClass(".score-part.exits", _("Points for connecting exits. Each group of connected exits scores the number of points based on number of exits in the group as shown in the table below. Train stations connects different different routes, but highway overpass not."), "");
      this.addTooltipToClass(".score-part.highway", _("Points for longest highway. Each pass through a field in the longest highway counts as one point. Some fields can be counted multiple times"), "");
      this.addTooltipToClass(".score-part.railroad", _("Points for longest railroad. Each pass through a field in the longest railroad counts as one point. Some fields can be counted multiple times"), "");
      this.addTooltipToClass(".score-part.central", _("Points for central fields. You get one point for each central field (marked with different color) on which you have drawn anything."), "");
      this.addTooltipToClass(".score-part.errors", _("Negative points for errors, Each incomplete route that isn't connected to the edge of the board (exit or not) is worth minus one point."), "");
      
      expansion == null && this.addTooltipToClass(".score-part.special", _("Expansion points. This points are awarded only when playing with an expansion"), "");
      expansion == 2 && this.addTooltipToClass(".score-part.special", _("Points for rivers. Only the best scoring river gives points. Each field in a river is worth 1 point. If the river is connected on both sides to the edge of the board, it is worh 3 more points"), "");
      expansion == 3 && this.addTooltipToClass(".score-part.special", _("Points for lakes. 1 point for each field in your smallest lake"), "");
      expansion == 4 && this.addTooltipToClass(".score-part.special", _("Points for lava. You get 5 points for each lava lake without open sides and additional 1 point for each field in largest lava lake."), "");
      expansion == 5 && this.addTooltipToClass(".score-part.special", _("Points for meteors. Each open connection that leads to a crater is worth 2 points and doesn't count as an error."), "");
      
      let dice = [
          this.format_block("jstpl_die", { route: 0, type: 0 }),
          this.format_block("jstpl_die", { route: 1, type: 0 }),
          this.format_block("jstpl_die", { route: 2, type: 0 }),
          this.format_block("jstpl_die", { route: 3, type: 0 }),
          this.format_block("jstpl_die", { route: 4, type: 0 }),
          this.format_block("jstpl_die", { route: 5, type: 0 }),
      ];
      this.addTooltipHtml("die-0", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      this.addTooltipHtml("die-1", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      this.addTooltipHtml("die-2", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);

      dice = [
        this.format_block("jstpl_die", { route: 6, type: 0 }),
        this.format_block("jstpl_die", { route: 6, type: 0 }),
        this.format_block("jstpl_die", { route: 7, type: 0 }),
        this.format_block("jstpl_die", { route: 7, type: 0 }),
        this.format_block("jstpl_die", { route: 8, type: 0 }),
        this.format_block("jstpl_die", { route: 8, type: 0 }),
      ];
      this.addTooltipHtml("die-3", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);

      if (expansion == 2){
        dice = [
          this.format_block("jstpl_die", { route: 0, type: 2 }),
          this.format_block("jstpl_die", { route: 1, type: 2 }),
          this.format_block("jstpl_die", { route: 2, type: 2 }),
          this.format_block("jstpl_die", { route: 3, type: 2 }),
          this.format_block("jstpl_die", { route: 3, type: 2 }),
          this.format_block("jstpl_die", { route: 3, type: 2 }),
        ];
        this.addTooltipHtml("die-4", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
        this.addTooltipHtml("die-5", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      } else if (expansion == 3){
        dice = [
          this.format_block("jstpl_die", { route: 0, type: 3 }),
          this.format_block("jstpl_die", { route: 1, type: 3 }),
          this.format_block("jstpl_die", { route: 2, type: 3 }),
          this.format_block("jstpl_die", { route: 3, type: 3 }),
          this.format_block("jstpl_die", { route: 4, type: 3 }),
          this.format_block("jstpl_die", { route: 5, type: 3 }),
        ];
        this.addTooltipHtml("die-4", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
        this.addTooltipHtml("die-5", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      } else if (expansion == 4){
        dice = [
          this.format_block("jstpl_die", { route: 0, type: 4 }),
          this.format_block("jstpl_die", { route: 0, type: 4 }),
          this.format_block("jstpl_die", { route: 1, type: 4 }),
          this.format_block("jstpl_die", { route: 2, type: 4 }),
          this.format_block("jstpl_die", { route: 3, type: 4 }),
          this.format_block("jstpl_die", { route: 4, type: 4 }),
        ];
        this.addTooltipHtml("die-4", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
        this.addTooltipHtml("die-5", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      } else if (expansion == 5){
        dice = [
          this.format_block("jstpl_die", { route: 0, type: 5 }),
          this.format_block("jstpl_die", { route: 0, type: 5 }),
          this.format_block("jstpl_die", { route: 0, type: 5 }),
          this.format_block("jstpl_die", { route: 1, type: 5 }),
          this.format_block("jstpl_die", { route: 1, type: 5 }),
          this.format_block("jstpl_die", { route: 2, type: 5 }),
        ];
        this.addTooltipHtml("die-4", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
        dice = [
          this.format_block("jstpl_die", { route: 3, type: 5 }),
          this.format_block("jstpl_die", { route: 4, type: 5 }),
          this.format_block("jstpl_die", { route: 5, type: 5 }),
          this.format_block("jstpl_die", { route: 6, type: 5 }),
          this.format_block("jstpl_die", { route: 7, type: 5 }),
          this.format_block("jstpl_die", { route: 7, type: 5 }),
        ];
        this.addTooltipHtml("die-5", this.format_block("jstpl_dice_tooltip", { dice: dice.join("") }), 1000);
      }
    },
    setRound: function(round, totalRounds = null){
      dojo.byId("round").innerHTML = round;
      totalRounds && (dojo.byId("total-rounds").innerHTML = totalRounds);
    },
    canUseSpecialRoutes: function(){
      return !this.specialRouteUsedThisTurn && this.usedSpecialRoutesNumber < 3 && this.isCurrentPlayerActive();
    },
    setupDiceUse: function() {
      this.dice.activate(this.routeSelected.bind(this));

      if (this.canUseSpecialRoutes()){
        this.boards
          .getPlayerBoard(this.player_id)
          .activateSpecialRoutes(this.routeSelected.bind(this));
      }
    },
    filterAvailableFieldsByRoute(selectedRoute) {      
      return this.availableFields.filter(function(field) {
        $routePossibilities = Object.values(field.possibleRoutes);
        $currentRoutePossibilities = $routePossibilities.filter(function(
          possibility
        ) {
          return (
            +possibility.route === +selectedRoute.route &&
            +possibility.routeType === +selectedRoute.routeType &&
            (possibility.normalRotations.length > 0 ||
              possibility.flippedRotations.length > 0)
          );
        });

        return $currentRoutePossibilities.length > 0;
      });
    },
    routeSelected: function(selectedRoute) {
      var playerBoard = this.boards.getPlayerBoard(this.player_id);

      if (selectedRoute.routeType === ROUTE_TYPE_SPECIAL) {
        this.dice.unselectAll();
      } else {
        playerBoard.unselectAllSpecialRoutes();
      }

      playerBoard.clearInteractiveField();

      var filteredFields = this.filterAvailableFieldsByRoute(selectedRoute);

      playerBoard.activateAvailableFields(
        filteredFields,
        selectedRoute,
        this.placedOnField.bind(this)
      );
    },
    placedOnField: function(field, selectedRoute) {
      var playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.clearInteractiveField();

      var fieldData = field.getData();
      var possibleRoutes = this.availableFields.filter(function(f) {
        return +f.x === +fieldData.x && +f.y === +fieldData.y;
      })[0].possibleRoutes;

      var currentRoutePossibilities = possibleRoutes.filter(function(p) {
        return (
          +p.route === +selectedRoute.route &&
          +p.routeType === +selectedRoute.routeType
        );
      })[0];

      var isFlipped = false,
        rotation = currentRoutePossibilities.normalRotations[0];
      if (currentRoutePossibilities.normalRotations.length === 0) {
        isFlipped = true;
        rotation = currentRoutePossibilities.flippedRotations[0];
      }

      const fieldRaw = {
        route: selectedRoute.route,
        routeType: selectedRoute.routeType,
        rotation: rotation,
        flipped: isFlipped,
        isLastMeteor: false
      };
      if (field.isSet()){
        fieldRaw.isLastMeteor = field.isLastMeteor();
        fieldRaw.isReplaced = true;
        fieldRaw.replacedData = fieldData;
        fieldRaw.replacedData.round = field.getRound();
        playerBoard.getSpecificFields([fieldData]).reset();
      }

      field.set(fieldRaw);
      field.makeInteractive(
        selectedRoute,
        currentRoutePossibilities,
        this.onConfirm.bind(this)
      );
    },
    onConfirm: function(field, selectedRoute) {
      field.turnOffInteractive();
      field.setCurrent();

      var playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.deactivateAllFields();
      var data = field.getData();
      var playerBoard = this.boards.getPlayerBoard(this.player_id);

      if (+selectedRoute.routeType === ROUTE_TYPE_LAVA && +selectedRoute.route === 5){
        this.disableVolcanoButton();
      } else if (+selectedRoute.routeType !== ROUTE_TYPE_SPECIAL) {
        this.dice.useDie(selectedRoute);
        data.dieId = selectedRoute.id
      } else {
        playerBoard.useSpecialRoute(selectedRoute);
        this.specialRouteUsedThisTurn = true;
        this.usedSpecialRoutesNumber++;

        data.specialId = selectedRoute.route;
      }

      
      if (this.checkAction('drawRoute')){
        this.dice.deactivate();
        playerBoard.deactivateSpecialRoutes();
        data.lock = true;
        this.ajaxcall(
          'railroadink/railroadink/drawRoute.html',
          data,
          this,
          function() {}
        );
      }
    },
    setSpecialRoutesUseForPlayerBoard: function(usedSpecial, board){
      board.unselectAllSpecialRoutes();
      board.unuseAllSpecialRoutes();

      if (!usedSpecial){
        usedSpecial = { routes: {} };
      }

      Object.values(usedSpecial.routes).forEach(function(special){
        board.useSpecialRoute({ route: special.id });
      });
      this.usedSpecialRoutesNumber = Object.values(usedSpecial.routes).length;
      this.specialRouteUsedThisTurn = usedSpecial.currentRoundUsed;

      if (this.canUseSpecialRoutes()){
        board.activateSpecialRoutes(this.routeSelected.bind(this));
      }
    },
    disableVolcanoButton: function(){
      dojo.addClass("add_volcano", "disabled");
    },
    enableVolcanoButton: function(){
      if (dojo.byId("add_volcano") !== null){
        dojo.removeClass("add_volcano", "disabled");
      }
    },
    disableDoneButton: function(){
      if (dojo.byId("done_button") !== null){
        dojo.addClass("done_button", "disabled");
      }
    },
    enableDoneButton: function(){
      if (dojo.byId("done_button") !== null){
        dojo.removeClass("done_button", "disabled");
      }
    },
    checkDoneButton: function(){
      this.canFinish ? this.enableDoneButton() : this.disableDoneButton();
    },
    ///////////////////////////////////////////////////
    //// Player's action

    /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
            */
    onAddVolcano: function() {
      var playerBoard = this.boards.getPlayerBoard(this.player_id);

      this.dice.unselectAll();
      playerBoard.unselectAllSpecialRoutes();

      playerBoard.clearInteractiveField();

      var selectedRoute = {
        route: 5, routeType: 4
      };

      var filteredFields = this.filterAvailableFieldsByRoute(selectedRoute);

      playerBoard.activateAvailableFields(
        filteredFields,
        selectedRoute,
        this.placedOnField.bind(this)
      );
    },
    
    onUndo: function(){
      this.checkAction('undoUseDice');
      this.ajaxcall(
        'railroadink/railroadink/undoUseDice.html',
        { lock: true },
        this,
        function() {}
      );
    },
    
    onRestart: function(){
      this.checkAction('restartUseDice');
      this.ajaxcall(
        'railroadink/railroadink/restartUseDice.html',
        { lock: true },
        this,
        function() {}
      );
    },

       
    onDone: function(){
      this.checkAction('doneUseDice');
      this.boards.getPlayerBoard(this.player_id).deactivateSpecialRoutes();
      this.dice.deactivate()
      this.ajaxcall(
        'railroadink/railroadink/doneUseDice.html',
        { lock: true },
        this,
        function() {}
      );
    },

    onAllowMeteor: function() {
      this.checkAction('allowMeteor');
      const playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.deactivateAllFields();
      playerBoard.deactivateSpecialRoutes();
      this.ajaxcall(
        'railroadink/railroadink/allowMeteor.html',
        { 
          lock: true,
        },
        this,
        function() {}
      );
    },
    
    onDecideMeteor: function(row, col) {
      this.checkAction('decideMeteor');
      const playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.deactivateAllFields();
      playerBoard.deactivateSpecialRoutes();
      this.ajaxcall(
        'railroadink/railroadink/decideMeteor.html',
        { 
          lock: true,
          x: row,
          y: col 
        },
        this,
        function() {}
      );
    },
    
    onDecidePrevent: function(selectedRoute) {
      this.checkAction('decidePrevent');

      const playerBoard = this.boards.getPlayerBoard(this.player_id);
      playerBoard.deactivateAllFields();
      playerBoard.useSpecialRoute(selectedRoute);
      playerBoard.deactivateSpecialRoutes();
      this.specialRouteUsedThisTurn = true;
      this.usedSpecialRoutesNumber++;
      this.ajaxcall(
        'railroadink/railroadink/decidePrevent.html',
        { 
          lock: true,
          route: selectedRoute.route 
        },
        this,
        function() {}
      );
    },

    /* Example:
        
        onMyMethodToCall1: function( evt )
        {
            console.log( 'onMyMethodToCall1' );
            
            // Preventing default browser reaction
            dojo.stopEvent( evt );

            // Check that this action is possible (see "possibleactions" in states.inc.php)
            if( ! this.checkAction( 'myAction' ) )
            {   return; }

            this.ajaxcall( "/railroadink/railroadink/myAction.html", { 
                                                                    lock: true, 
                                                                    myArgument1: arg1, 
                                                                    myArgument2: arg2,
                                                                    ...
                                                                 }, 
                         this, function( result ) {
                            
                            // What to do after the server call if it succeeded
                            // (most of the time: nothing)
                            
                         }, function( is_error) {

                            // What to do after the server call in anyway (success or failure)
                            // (most of the time: nothing)

                         } );        
        },        
        
        */

    ///////////////////////////////////////////////////
    //// Reaction to cometD notifications

    /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your railroadink.game.php file.
        
        */
    setupNotifications: function() {
      console.log('notifications subscriptions setup');

      dojo.subscribe( 'newAvailableFields', this, "notif_newAvailableFields");
      dojo.subscribe( 'newRound', this, "notif_newRound");
      dojo.subscribe( 'restartUseDice', this, "notif_restartUseDice");
      dojo.subscribe( 'undoUseDice', this, "notif_undoUseDice");
      dojo.subscribe( 'meteorStrikes', this, "notif_meteorStrikes");
      dojo.subscribe( 'updatePlayerBoard', this, "notif_updatePlayerBoard");
      dojo.subscribe( 'playerFinished', this, "notif_playerFinished");
      this.notifqueue.setSynchronous( 'playerFinished', 500 );
      dojo.subscribe( 'spectatorUpdate', this, "notif_spectatorUpdate");
      //this.notifqueue.setIgnoreNotificationCheck( 'playerFinished', () => !this.isSpectator );


      // TODO: here, associate your game notifications with local methods

      // Example 1: standard notification handling
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );

      // Example 2: standard notification handling + tell the user interface to wait
      //            during 3 seconds after calling the method in order to let the players
      //            see what is happening in the game.
      // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );
      // this.notifqueue.setSynchronous( 'cardPlayed', 3000 );
      //
    },

    notif_playerFinished: function(notif){
      this.boards.getPlayerBoard(notif.args.playerId).setIdle();
    },
    
    notif_spectatorUpdate: function(notif){
      Object.values(notif.args.boards).forEach(board => {
        var playerBoard = this.boards.getPlayerBoard(board.playerId)
        playerBoard.resetSpecificFields(board.fields);
        playerBoard.setScore(board.score);  
      });
    },

    notif_updatePlayerBoard: function(notif){
      var board = this.boards.getPlayerBoard(notif.args.board.playerId)
      board.resetSpecificFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);

      this.scoreCtrl[ notif.args.board.playerId ].setValue( notif.args.board.score.total );

      if (notif.args.specialUsage){
        Object.values(notif.args.specialUsage.routes).forEach(function(special){
          board.useSpecialRoute({ route: special.id });
        });
      }
    },

    notif_restartUseDice: function(notif){
      this.dice.unuseAll();
      var board = this.boards.getPlayerBoard(this.player_id)
      board.resetFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);
      
      this.availableFields = Object.values(
        notif.args.availableFields
      );
        
      this.setSpecialRoutesUseForPlayerBoard(notif.args.usedSpecial[this.player_id], board);
      this.enableVolcanoButton()

      this.canFinish = notif.args.canFinish; 
      this.checkDoneButton();
    },

    notif_undoUseDice: function(notif){
      var board = this.boards.getPlayerBoard(this.player_id)
      board.resetFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);
      
      this.availableFields = Object.values(
        notif.args.availableFields
      );
        
      this.setSpecialRoutesUseForPlayerBoard(notif.args.usedSpecial[this.player_id], board);
      var usedDice = notif.args.usedDice[this.player_id] || {};
      this.dice.unuseAll();

      Object.values(usedDice).forEach(this.dice.useDie);

      if (notif.args.undoVolcano){
        this.enableVolcanoButton()
      }

      this.canFinish = notif.args.canFinish; 
      this.checkDoneButton();
    },
    
    notif_meteorStrikes: function(notif){
      var board = this.boards.getPlayerBoard(this.player_id)
      board.resetFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);
    },

    notif_newAvailableFields: function(notif){
      console.log( 'notif_newAvailableFields' );
      console.log( notif );

      this.availableFields = Object.values(
        notif.args.availableFields
      );
    
      this.setupDiceUse();

      var board = this.boards.getPlayerBoard(this.player_id)
      board.resetFields(notif.args.board.fields);
      board.setScore(notif.args.board.score);

      this.canFinish = notif.args.canFinish; 
      this.checkDoneButton();
    },

    notif_newRound: function(notif){
      console.log( 'notif_newRound' );
      this.setRound(notif.args.round);
      this.boards.setAllBoardsActive();

      this.boards.getPlayerBoard(this.player_id).getAllFields().removeCurrentRoundHighlights();
      
      var usedDice = notif.args.usedDice[this.player_id] || {};
      this.dice.reset();
      this.dice.set(notif.args.dice, Object.values(usedDice));
    },

    // TODO: from this point and below, you can write your game notifications handling methods

    /*
        Example:
        
        notif_cardPlayed: function( notif )
        {
            console.log( 'notif_cardPlayed' );
            console.log( notif );
            
            // Note: notif.args contains the arguments specified during you "notifyAllPlayers" / "notifyPlayer" PHP call
            
            // TODO: play the card in the user interface.
        },    
        
        */
  });
});
