<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * railroadink implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * stats.inc.php
 *
 * railroadink game statistics description
 *
 */

/*
    In this file, you are describing game statistics, that will be displayed at the end of the
    game.
    
    !! After modifying this file, you must use "Reload  statistics configuration" in BGA Studio backoffice
    ("Control Panel" / "Manage Game" / "Your Game")
    
    There are 2 types of statistics:
    _ table statistics, that are not associated to a specific player (ie: 1 value for each game).
    _ player statistics, that are associated to each players (ie: 1 value for each player in the game).

    Statistics types can be "int" for integer, "float" for floating point values, and "bool" for boolean
    
    Once you defined your statistics there, you can start using "initStat", "setStat" and "incStat" method
    in your game logic, using statistics names defined below.
    
    !! It is not a good idea to modify this file when a game is running !!

    If your game is already public on BGA, please read the following before any change:
    http://en.doc.boardgamearena.com/Post-release_phase#Changes_that_breaks_the_games_in_progress
    
    Notes:
    * Statistic index is the reference used in setStat/incStat/initStat PHP method
    * Statistic index must contains alphanumerical characters and no space. Example: 'turn_played'
    * Statistics IDs must be >=10
    * Two table statistics can't share the same ID, two player statistics can't share the same ID
    * A table statistic can have the same ID than a player statistics
    * Statistics ID is the reference used by BGA website. If you change the ID, you lost all historical statistic data. Do NOT re-use an ID of a deleted statistic
    * Statistic name is the English description of the statistic as shown to players
    
*/

require_once('modules/RRIStats.php');

$stats_type = array(

    // Statistics global to table
    "table" => array(
    ),
    
    // Statistics existing for each player
    "player" => array(
        RRIStats::TOTAL_POINTS => array("id"=> 10,
                    "name" => totranslate("Total points"),
                    "type" => "int" ),
        RRIStats::POINTS_FROM_EXITS => array("id"=> 11,
                    "name" => totranslate("Points for connecting exits"),
                    "type" => "int" ),
        RRIStats::POINTS_FROM_LONGEST_HIGHWAY => array("id"=> 12,
                    "name" => totranslate("Points for longest highway"),
                    "type" => "int" ),
        RRIStats::POINTS_FROM_LONGEST_RAILROAD => array("id"=> 13,
                    "name" => totranslate("Points for longest railroad"),
                    "type" => "int" ),
        RRIStats::POINTS_FROM_CENTER => array("id"=> 14,
                    "name" => totranslate("Points for central fields"),
                    "type" => "int" ),
        RRIStats::POINTS_FROM_EXPANSION => array("id"=> 15,
                    "name" => totranslate("Points for expansion"),
                    "type" => "int" ),
        RRIStats::LOST_POINTS_ERRORS => array("id"=> 16,
                    "name" => totranslate("Lost points for errors"),
                    "type" => "int" ),
        RRIStats::USED_SPECIAL_ROUTES => array("id"=> 17,
                    "name" => totranslate("Number of special routes used"),
                    "type" => "int" ),
        RRIStats::SPECIAL_ROUTE_LAST_ROUND => array("id"=> 18,
                    "name" => totranslate("Round in which the last special route is used"),
                    "type" => "int" ),
        RRIStats::TOTAL_EXIT_GROUPS => array("id"=> 19,
                    "name" => totranslate("Total exit groups"),
                    "type" => "int" ),
        RRIStats::TWO_EXITS_CONNECTED => array("id"=> 20,
                    "name" => totranslate("Number of exit groups of length 2"),
                    "type" => "int" ),
        RRIStats::THREE_EXITS_CONNECTED => array("id"=> 21,
                    "name" => totranslate("Number of exit groups of length 3"),
                    "type" => "int" ),
        RRIStats::FOUR_EXITS_CONNECTED => array("id"=> 22,
                    "name" => totranslate("Number of exit groups of length 4"),
                    "type" => "int" ),
        RRIStats::FIVE_EXITS_CONNECTED => array("id"=> 23,
                    "name" => totranslate("Number of exit groups of length 5"),
                    "type" => "int" ),
        RRIStats::SIX_EXITS_CONNECTED => array("id"=> 24,
                    "name" => totranslate("Number of exit groups of length 6"),
                    "type" => "int" ),
        RRIStats::SEVEN_EXITS_CONNECTED => array("id"=> 25,
                    "name" => totranslate("Number of exit groups of length 7"),
                    "type" => "int" ),
        RRIStats::EIGHT_EXITS_CONNECTED => array("id"=> 26,
                    "name" => totranslate("Number of exit groups of length 8"),
                    "type" => "int" ),
        RRIStats::NINE_EXITS_CONNECTED => array("id"=> 27,
                    "name" => totranslate("Number of exit groups of length 9"),
                    "type" => "int" ),
        RRIStats::TEN_EXITS_CONNECTED => array("id"=> 28,
                    "name" => totranslate("Number of exit groups of length 10"),
                    "type" => "int" ),
        RRIStats::ELEVEN_EXITS_CONNECTED => array("id"=> 29,
                    "name" => totranslate("Number of exit groups of length 11"),
                    "type" => "int" ),
        RRIStats::TWELVE_EXITS_CONNECTED => array("id"=> 30,
                    "name" => totranslate("Number of exit groups of length 12"),
                    "type" => "int" ),
        RRIStats::PLAYED_RIVER_EXPANSION => array("id"=> 31,
                    "name" => totranslate("Played river expansion"),
                    "type" => "bool" ),
        RRIStats::PLAYED_LAKE_EXPANSION => array("id"=> 32,
                    "name" => totranslate("Played lake expansion"),
                    "type" => "bool" ),
        RRIStats::PLAYED_LAVA_EXPANSION => array("id"=> 33,
                    "name" => totranslate("Played lava expansion"),
                    "type" => "bool" ),
        RRIStats::PLAYED_METEOR_EXPANSION => array("id"=> 34,
                    "name" => totranslate("Played meteor expansion"),
                    "type" => "bool" ),
    )

);
