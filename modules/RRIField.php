<?php

class RRIField
{
    private $route;
    private $rotation;
    private $routeType;
    private $flipped;
    private $x;
    private $y;
    private $round;
    private $isLastMeteor;

    const TYPE_REGULAR = 0;
    const TYPE_SPECIAL = 1;
    const TYPE_RIVER = 2;
    const TYPE_LAKE = 3;
    const TYPE_LAVA = 4;
    const TYPE_METEOR = 5;

    private static $routes = [
        0 => [
            "sides" => ["R", "B", "B", "R"],
            "rotations" => 4,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        1 => [
            "sides" => ["R", "R", "B", "R"],
            "rotations" => 4,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        2 => [
            "sides" => ["R", "B", "R", "B"],
            "rotations" => 2,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        3 => [
            "sides" => ["H", "B", "B", "H"],
            "rotations" => 4,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        4 => [
            "sides" => ["H", "H", "B", "H"],
            "rotations" => 4,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        5 => [
            "sides" => ["H", "B", "H", "B"],
            "rotations" => 2,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        6 => [
            "sides" => ["H", "R", "H", "R"],
            "rotations" => 2,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => true,
            "parts" => [
                [
                    "routeType" => 0,
                    "route" => 5,
                    "rotationOffset" => 0
                ],
                [
                    "routeType" => 0,
                    "route" => 2,
                    "rotationOffset" => 1
                ],
            ]
        ],
        7 => [
            "sides" => ["R", "B", "H", "B"],
            "rotations" => 4,
            "station" => true,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        8 => [
            "sides" => ["R", "B", "B", "H"],
            "rotations" => 2,
            "station" => true,
            "flips" => true,
            "mustConnect" => true,
            "splits" => false,
        ],
    ];
    private static $specialRoutes = [
        0 => [
            "sides" => ["H", "H", "R", "H"],
            "rotations" => 4,
            "station" => true,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        1 => [
            "sides" => ["H", "R", "R", "R"],
            "rotations" => 4,
            "station" => true,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        2 => [
            "sides" => ["H", "H", "H", "H"],
            "rotations" => 0,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        3 => [
            "sides" => ["R", "R", "R", "R"],
            "rotations" => 0,
            "station" => false,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        4 => [
            "sides" => ["H", "R", "R", "H"],
            "rotations" => 4,
            "station" => true,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
        5 => [
            "sides" => ["H", "R", "H", "R"],
            "rotations" => 2,
            "station" => true,
            "flips" => false,
            "mustConnect" => true,
            "splits" => false,
        ],
    ];

    private static $expansions = [
        self::TYPE_RIVER => [
            0 => [
                "sides" => ["W", "B", "W", "B"],
                "rotations" => 2,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            1 => [
                "sides" => ["W", "R", "W", "R"],
                "rotations" => 2,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => true,
                "parts" => [
                    [
                        "routeType" => 0,
                        "route" => 2,
                        "rotationOffset" => 1
                    ],
                    [
                        "routeType" => 2,
                        "route" => 0,
                        "rotationOffset" => 0
                    ],
                ]
            ],
            2 => [
                "sides" => ["W", "H", "W", "H"],
                "rotations" => 2,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => true,
                "parts" => [
                    [
                        "routeType" => 2,
                        "route" => 0,
                        "rotationOffset" => 0
                    ],
                    [
                        "routeType" => 0,
                        "route" => 5,
                        "rotationOffset" => 1
                    ],
                ]
            ],
            3 => [
                "sides" => ["W", "W", "B", "B"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
        ],
        self::TYPE_LAKE => [
            0 => [
                "sides" => ["B", "L", "L", "B"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            1 => [
                "sides" => ["B", "B", "L", "B"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            2 => [
                "sides" => ["H", "B", "L", "B"],
                "rotations" => 4,
                "station" => true,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            3 => [
                "sides" => ["R", "B", "L", "B"],
                "rotations" => 4,
                "station" => true,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            4 => [
                "sides" => ["R", "L", "L", "H"],
                "rotations" => 4,
                "station" => true,
                "flips" => true,
                "mustConnect" => false,
                "splits" => false,
            ],
            5 => [
                "sides" => ["L", "L", "B", "L"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
            6 => [
                "sides" => ["L", "L", "L", "L"],
                "rotations" => 1,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
            ],
        ],
        self::TYPE_METEOR => [
            0 => [
                "sides" => ["B", "B", "B", "B"],
                "rotations" => 1,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
                "canOverwrite" => true,
            ]
        ],
        self::TYPE_LAVA => [
            0 => [
                "sides" => ["B", "M", "M", "B"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => false,
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ],
            1 => [
                "sides" => ["M", "M", "B", "M"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => false,
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ],
            2 => [
                "sides" => ["B", "R", "M", "R"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => true,
                "parts" => [
                    [
                        "routeType" => self::TYPE_LAVA,
                        "route" => 6,
                        "rotationOffset" => 0
                    ],
                    [
                        "routeType" => 0,
                        "route" => 2,
                        "rotationOffset" => 1
                    ],
                ],
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ],
            3 => [
                "sides" => ["M", "B", "M", "B"],
                "rotations" => 2,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => false,
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ],
            4 => [
                "sides" => ["B", "H", "M", "H"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => true,
                "parts" => [
                    [
                        "routeType" => self::TYPE_LAVA,
                        "route" => 6,
                        "rotationOffset" => 0
                    ],
                    [
                        "routeType" => 0,
                        "route" => 5,
                        "rotationOffset" => 1
                    ],
                ],
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ],
            5 => [
                "sides" => ["M", "M", "M", "M"],
                "rotations" => 1,
                "station" => false,
                "flips" => false,
                "mustConnect" => false,
                "splits" => false,
                "canOverwrite" => true,
            ],
            6 => [
                "sides" => ["B", "B", "M", "B"],
                "rotations" => 4,
                "station" => false,
                "flips" => false,
                "mustConnect" => true,
                "splits" => false,
                "mustConnectToType" => "M",
                "canOverwrite" => true,
            ]
        ],
    ];

    private static $sideThatCanMismatch = [ "L", "M" ];
    private static $sideThatDoesntCountAsError = [ "L" ];

    public function __construct($raw)
    {
        $raw = array_merge([
            "route" => null,
            "route_type" => null,
            "rotate" => null,
            "is_flipped" => null,
            "x" => null,
            "y" => null,
            "round" => null,
            "is_last_meteor" => null
        ], $raw);

        $this->route = intval($raw["route"]);
        $this->routeType = intval($raw["route_type"]);
        $this->rotation = self::getNormalizedRotation(intval($raw["rotate"]));
        $this->flipped = intval($raw["is_flipped"]) === 1;
        $this->x = intval($raw["x"]);
        $this->y = intval($raw["y"]);
        $this->round = $raw["round"] !== null ? intval($raw["round"]) : null;
        $this->isLastMeteor = intval($raw["is_last_meteor"]);
    }

    public function serialize($currentRound = null)
    {
        return
            [
                'route'   => $this->route,
                'routeType'   => $this->routeType,
                'rotation'   => $this->rotation,
                'flipped'   => $this->flipped,
                'x'   => $this->x,
                'y'   => $this->y,
                'round' => $this->round,
                'isCurrent' => $currentRound != null && $currentRound == $this->round,
                'isLastMeteor' => $this->isLastMeteor === 1
            ];
    }
    
    public function serializeFieldCoordinates()
    {
        return
            [
                'x'   => $this->x,
                'y'   => $this->y
            ];
    }

    private function getRouteDefinition(){
        return $this->getAllRoutesWithExpansion($this->routeType)[$this->routeType][$this->route];
    }

    public function isSplitable(){
        return $this->getRouteDefinition()["splits"];
    }

    public function canOverwrite(){
        $definition = $this->getRouteDefinition();

        return array_key_exists("canOverwrite", $definition) && $definition["canOverwrite"];
    }

    public function split(){
        if ($this->isSplitable()){
            $parts = $this->getRouteDefinition()["parts"];

            return array_map(function($part){
                return new RRIField([
                    "route" => $part["route"],
                    "route_type" => $part["routeType"],
                    "rotate" => $this->rotation + $part["rotationOffset"],
                    "is_flipped" => $this->flipped,
                    "x" => $this->x,
                    "y" => $this->y,
                    "round" => $this->round,
                ]);
            }, $parts);
        }

        throw new Error("Cannot split field with routeType ".$this->routeType." and route ".$this->route);
    }

    public static function getSpecialRoutes()
    {
        return self::$specialRoutes;
    }

    private static function rotateSides($sides, $rotations)
    {
        if ($rotations === 0) {
            return $sides;
        }
        array_unshift($sides, array_pop($sides));
        return self::rotateSides($sides, $rotations - 1);
    }

    private static function flipSides($sides)
    {
        $temp = $sides[1];
        $sides[1] = $sides[3];
        $sides[3] = $temp;

        return $sides;
    }

    private static function getNormalizedRotation($rotation)
    {
        $normalized = $rotation % 4;

        return $normalized < 0 ? $normalized + 4 : $normalized;
    }

    public function hasStation(){
        $route = $this->getRoutesByType($this->routeType)[$this->route];

        return $route["station"];
    }

    private function getRoutesByType($type){
        if ($type < 2){
            return self::getAllRoutesGroupedByType()[$type];
        }

        return self::$expansions[$type];
    }

    private function getFieldRoutes(){
        return $this->getRoutesByType($this->routeType);
    }

    public function checkIfConnected($direction, $type){
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        switch ($direction) {
            case 'N':
                return $south == $type;           
            case 'S':
                return $north == $type;           
            case 'E':
                return $west == $type;           
            case 'W':
                return $east == $type;           
            default:
                return false;
        }
    }

    private function getRotatedAndFlippedSides() {
        $route = $this->getFieldRoutes()[$this->route];
        $sides = $route["sides"];
        if ($this->flipped) {
            $sides = self::flipSides($sides);
        }

        $sides = self::rotateSides($sides, $this->rotation);

        return $sides;
    }

    public function getFieldDirections($includeOffBoard = false)
    {
        $directions = [];

        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        if ($north !== "B" && ($this->y > 0 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x,
                    "y" => $this->y - 1
                ],
                "direction" => "N",
                "type" => $north
            ];
        }
        if ($east !== "B" && ($this->x < 6 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x + 1,
                    "y" => $this->y
                ],
                "direction" => "E",
                "type" => $east
            ];
        }
        if ($south !== "B" && ($this->y < 6 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x,
                    "y" => $this->y + 1
                ],
                "direction" => "S",
                "type" => $south
            ];
        }
        if ($west !== "B" && ($this->x > 0 || $includeOffBoard)) {
            $directions[] = [
                "to" => [
                    "x" => $this->x - 1,
                    "y" => $this->y
                ],
                "direction" => "W",
                "type" => $west
            ];
        }

        return $directions;
    }

    public function checkAgainstRestrictions($restrictions) {
        $route = $this->getFieldRoutes()[$this->route];
        return self::checkRouteRotationAgainsRestrictions($route, $this->rotation, $this->flipped, $restrictions);
    }

    private static function getSideForDirection($sides, $positions)
    {
        switch ($positions) {
            case 'N':
                return $sides[0];
            case 'E':
                return $sides[1];
            case 'S':
                return $sides[2];
            case 'W':
                return $sides[3];

            default:
                return "B";
        }
    }

    private static function checkRouteRotationAgainsRestrictions($route, $rotation, $flipped, $restrictions)
    {
        $sides = $route["sides"];
        if ($flipped) {
            $sides = self::flipSides($sides);
        }
        if ($rotation > 0) {
            $sides = self::rotateSides($sides, $rotation);
        }

        $isConnected = false;

        $connectedToType = array_key_exists("mustConnectToType", $route) ? $route["mustConnectToType"] : null;
        $isConnectedToType = $connectedToType === null;

        foreach ($restrictions as $direction => $restriction) {
            $sideForDirection = self::getSideForDirection($sides, $direction);

            if ($sideForDirection === $restriction) {
                $isConnected = true;

                if ($restriction === $connectedToType){
                    $isConnectedToType = true;
                }
            }

            if ($restriction !== $sideForDirection 
                && $sideForDirection !== "B" 
                && array_search($sideForDirection, self::$sideThatCanMismatch) === false
                && array_search($restriction, self::$sideThatCanMismatch) === false ) {
                return false;
            }
        }

        return ($isConnected && $isConnectedToType) || !$route["mustConnect"];
    }

    private static function checkRouteAgainstRestriction($route, $flipped, $restrictions)
    {
        $rotations = [];
        for ($i = 0; $i < 4; $i++) {
            if (self::checkRouteRotationAgainsRestrictions($route, $i, $flipped, $restrictions)) {
                $rotations[] = $i;
            }
        }

        return $rotations;
    }

    private static function isRouteConnectionPossible($sides, $restrictionValues)
    {
        return sizeof(array_intersect($sides, $restrictionValues)) > 0;
    }

    private static function getAllRoutesGroupedByType() {
        return [
            self::TYPE_REGULAR => self::$routes,
            self::TYPE_SPECIAL => self::$specialRoutes,
        ];
    }

    private static function getAllRoutesWithExpansion($expansionType = 0) {
        if ($expansionType < 2){
            return self::getAllRoutesGroupedByType();
        }

        return self::getAllRoutesGroupedByType() + self::getExpansionRoutes($expansionType);
    }

    private static function getExpansionRoutes($expansionType){
        return [$expansionType => self::$expansions[$expansionType]];
    }

    /**
     * Gets the possible routes type that could be placed on a board 
     * field with certain restrictions.
     */
    public static function calculatePossibleRoutes($routes, $availableField)
    {
        $restrictions = $availableField["restrictions"];
        $possibleRoutes = [];

        foreach ($routes as $routeType => $currentRoutes) {
            foreach ($currentRoutes as $id => $route) {
                if ($routeType == self::TYPE_METEOR) {
                    continue;
                }

                if ($routeType == self::TYPE_LAVA && $id == 5){
                    if (!$availableField["isVolcanoPossible"]) {
                        continue;
                    }
                }

                if (array_key_exists("onlyRewritable", $availableField) && $availableField["onlyRewritable"] === true){
                    if (!array_key_exists("canOverwrite", $route) || $route["canOverwrite"] !== true){
                        continue;
                    }
                }

                $routePositions = [
                    "route" => $id,
                    "routeType" => $routeType,
                    "normalRotations" => [],
                    "flippedRotations" => []
                ];

                if (self::isRouteConnectionPossible($route["sides"], array_values($restrictions)) || !$route["mustConnect"]) {
                    $routePositions["normalRotations"] = self::checkRouteAgainstRestriction($route, false, $restrictions);
                    if ($route["flips"]) {
                        $routePositions["flippedRotations"] = self::checkRouteAgainstRestriction($route, true, $restrictions);
                    }
                }
                $possibleRoutes[] = $routePositions;
            }
        }

        return $possibleRoutes;
    }
    
    public static function getPossibleRoutes($availableField, $expansion = 0)
    {
        return self::calculatePossibleRoutes(self::getAllRoutesWithExpansion($expansion), $availableField);
    }

    public static function getPossibleExpansionRoutes($expansion){
        return self::calculatePossibleRoutes(self::getExpansionRoutes($expansion), [ "restrictions" => [] ]);
    }

    public function getDirectionsFrom($direction)
    {
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        $type = $this->getConnectionType($direction);

        $nextDirections = [];

        if ($north === $type && $north !== "B" && $direction !== "N"){
            $nextDirections[] = "N";
        }
        if ($south === $type && $south !== "B" && $direction !== "S"){
            $nextDirections[] = "S";
        }
        if ($west === $type && $west !== "B" && $direction !== "W"){
            $nextDirections[] = "W";
        }
        if ($east === $type && $east !== "B" && $direction !== "E"){
            $nextDirections[] = "E";
        }

        return $nextDirections;
    }

    public function getNeighbour($direction){
        $x = $this->x;
        $y = $this->y;
        
        if ($direction === "N"){
            $y--;
        }
        if ($direction === "S"){
            $y++;
        }
        if ($direction === "E"){
            $x++;
        }
        if ($direction === "W"){
            $x--;
        }

        $key = strval($x.$y);

        return compact("x", "y", "key");
    }

    public function getConnectionType($direction)
    {
        [$north, $east, $south, $west] = $this->getRotatedAndFlippedSides();

        switch ($direction) {
            case 'N':
                return $north;
            case 'S':
                return $south;
            case 'E':
                return $east;
            case 'W':
                return $west;
            default:
                # code...
                break;
        }
    }

    public function isSideConnectionTypeError($direction){
        return array_search($this->getConnectionType($direction), self::$sideThatDoesntCountAsError) === false;
    }

    public function getRouteType(){
        return $this->routeType;
    }
    
    public function getRoute(){
        return $this->route;
    }

    /**
     * Get the value of x
     */ 
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set the value of x
     *
     * @return  self
     */ 
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get the value of y
     */ 
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set the value of y
     *
     * @return  self
     */ 
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get the value of y
     */ 
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set the value of y
     *
     * @return  self
     */ 
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }
    
    /**
     * Get the value of isLastMeteor
     */ 
    public function isLastMeteor()
    {
        return $this->isLastMeteor == 1;
    }

    public function getIsLastMeteor()
    {
        return $this->isLastMeteor;
    }

    /**
     * Set the value of isLastMeteor
     *
     * @return  self
     */ 
    public function setIsLastMeteor($isLastMeteor)
    {
        $this->isLastMeteor = $isLastMeteor;

        return $this;
    }

    public function getRotation(){
        return $this->rotation;
    }

    public function isFlipped(){
        return $this->flipped;
    }
}
