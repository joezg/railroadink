<?php

class RRIGraph {

    private $nodes;

    public function __construct($options) {
        $options = array_merge([
            "fields" => [],
            "addEntryNodes" => false
        ], $options);

        [ "fields" => $fields, "addEntryNodes" => $addEntryNodes ] = $options;
        $this->makeGraph($fields, $addEntryNodes);;
    }

    private function fieldKey(int $x, int $y)
    {
        return $x . $y;
    }

    private function getNode(int $x, int $y, string $direction = null){
        $key = $this->fieldKey($x, $y);
        if (array_key_exists($key, $this->nodes)){
            return $this->nodes[$key];
        }

        $filteredKeys = array_values(array_filter(array_keys($this->nodes), function($nodeKey) use ($direction, $key){
            return strpos($nodeKey, $direction) !== false && strpos($nodeKey, $key) === 0;
        }));

        if (sizeof($filteredKeys) > 1){
            throw new BgaUserException("Graph is invalid!");
        }

        if (sizeof($filteredKeys) === 0){
            return null;
        }

        return $this->nodes[$filteredKeys[0]];
    }

    public function getAllNodes(int $x, int $y){
        $key = $this->fieldKey($x, $y);

        $filteredNodes = [];
        foreach ($this->nodes as $nodeKey => $node) {
            if (strpos($nodeKey, $key) === 0){
                $filteredNodes[$nodeKey] = $node;
            }
        }

        return $filteredNodes;
    }

    public function getFreeEndNodes(){
        return array_filter($this->nodes, function($node){
            return $node->isFreeEnd();
        });
    }
    
    public function getNodesWithStation(){
        return array_filter($this->nodes, function($node){
            return $node->getField()->hasStation();
        });
    }

    public function getNodesWithSpecificRouteType($routeType){
        return array_filter($this->nodes, function($node) use ($routeType) {
            return $node->getField()->getRouteType() == $routeType;
        });
    }
    
    private function makeGraph($fields, $addEntryNodes) {
        $this->nodes = [];

        //Create nodes
        for ($x=0; $x<7; $x++) { 
            for ($y=0; $y<7; $y++) { 
                $key = $this->fieldKey($x, $y);

                if (array_key_exists($key, $fields)) {
                    $field = $fields[$key];

                    if ($field->isSplitable()){
                        $parts = $field->split();

                        foreach ($parts as $part) {
                            $partKey = $this->fieldKey($x, $y);
                            foreach (["N", "E", "S", "W"] as $direction) {
                                if ($part->getConnectionType($direction) !== "B"){
                                    $partKey .= $direction;
                                }
                            }
                            
                            $this->nodes[$partKey] = new RRINode($part);
                        }
                    } else {
                        $node = new RRINode($fields[$key]);
                        $this->nodes[$this->fieldKey($x, $y)] = $node;
                    }
                    
                }

                if ($addEntryNodes){

                    if (array_search($x, [0, 6]) !== false && array_search($y, [1, 3, 5]) !== false){
                        $offBoardX = $x === 0 ? -1 : 7;
                        $field = new RRIField([
                            "x" => $offBoardX, 
                            "y" => $y, 
                            "route_type" => 0, 
                            "route" => $y === 3 ? 5 : 2,
                            "rotate" => 1
                        ]);
                        $node = new RRINode($field);
                        $this->nodes[$this->fieldKey($offBoardX, $y)] = $node;
                    }
                    
                    if (array_search($y, [0, 6]) !== false && array_search($x, [1, 3, 5]) !== false){
                        $offBoardY = $y === 0 ? -1 : 7;
                        $field = new RRIField([
                            "x" => $x, 
                            "y" => $offBoardY, 
                            "route_type" => 0, 
                            "route" => $x === 3 ? 2 : 5
                        ]);
                        $node = new RRINode($field);
                        $this->nodes[$this->fieldKey($x, $offBoardY)] = $node;
                    }
                }

            }
        }

        //Add edges
        foreach ($this->nodes as $node) {
            $includeOffBoard = true;
            $directions = $node->getField()->getFieldDirections($includeOffBoard);

            foreach ($directions as $next) {
                $x = $next["to"]["x"];
                $y = $next["to"]["y"];
                $type = $next["type"];
                $direction = $next["direction"];

                $nextNode = $this->getNode($x, $y, $this->getOppositeDirection($direction));
                
                if ($nextNode !== null){
                    if ($nextNode->getField()->checkIfConnected($direction, $type)){
                        $node->addEdge($direction, $nextNode);
                        continue;
                    }
                }

                $node->addUnconnectedDirection($direction);
            }

        }
    }

    private function getOppositeDirection($dir)
    {
        switch ($dir) {
            case 'N':
                return "S";
            case 'S':
                return "N";
            case 'E':
                return "W";
            case 'W':
                return "E";
            default:
                return null;
        }
    }
}