<?php

class RRIBoard
{
    private $playerId;
    private $fields = [];
    private $expansion = 0;

    const EXIT_SCORE_MAP = [
        0 => 0,
        1 => 0,
        2 => 4,
        3 => 8,
        4 => 12,
        5 => 16,
        6 => 20,
        7 => 24,
        8 => 28,
        9 => 32,
        10 => 36,
        11 => 40,
        12 => 45,
    ];

    public function __construct(int $playerId, ?int $expansion)
    {
        $this->playerId = $playerId;
        $this->expansion = $expansion;
    }

    public function addFields($fields)
    {
        foreach ($fields as $raw) {
            $this->addField($raw);
        }
    }

    public function addField($raw)
    {
        $field = new RRIField($raw);
        $this->fields[$this->fieldKey($raw["x"], $raw["y"])] = $field;
    }

    public function replaceField($x, $y, $field){
        $this->fields[$this->fieldKey($x, $y)] = $field;
    }

    private function fieldKey(int $x, int $y)
    {
        return $x . $y;
    }

    public function getPlayerId(){
        return $this->playerId;
    }

    public function removeFieldsForRound($round){
        $this->fields = array_filter($this->fields, function($field) use ($round) {
            return $field->getRound() != $round;
        });
    }

    public function removeUnconfirmedFields(){
        $this->removeFieldsForRound(null);
    }

    private function serializeFields($fields, $currentRound = null){
        return array_values(array_map(function ($field) use ($currentRound) {
            return $field->serialize($currentRound);
        }, $fields));
    }

    private function serializeFieldCoordinates($fields) {
        return array_values(array_map(function ($field) {
            return $field->serializeFieldCoordinates();
        }, $fields));
    }

    public function filterLastRoundAndSerialize($currentRound = null){
        $currentRoundFields = array_filter($this->fields, function($field) use ($currentRound){
            return $currentRound == $field->getRound();
        });

        return
            [
                "playerId" => $this->playerId,
                'fields'   => $this->serializeFields($currentRoundFields, $currentRound),
                "score" => $this->getScore()
            ];
    }

    public function serialize($currentRound = null)
    {
        return
            [
                "playerId" => $this->playerId,
                'fields'   => $this->serializeFields($this->fields, $currentRound),
                "score" => $this->getScore()
            ];
    }

    public function isFieldExists($x, $y)
    {
        return array_key_exists($this->fieldKey($x, $y), $this->fields);
    }

    public function getAvailableFields()
    {
        $available = [];

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => true]);
        $freeEndNodes = $graph->getFreeEndNodes();

        foreach ($freeEndNodes as $node) {
            $insideUnconnected = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());
            foreach ($insideUnconnected as $unconnected) {
                ["x" => $x, "y" => $y, "key" => $key ] = $node->getField()->getNeighbour($unconnected);
                
                $neighbouringField = $this->fields[$this->fieldKey($x, $y)] ?? null;
                if (!$this->isFieldExists($x, $y) 
                    || $neighbouringField->getRouteType() == RRIField::TYPE_METEOR
                    || ($node->getField()->canOverwrite() && $neighbouringField->getRouteType() !== $node->getField()->getRouteType())){
                
                    $currentAvailable = [
                        "x" => $x,
                        "y" => $y,
                        "restrictions" => [],
                        "isVolcanoPossible" => $this->isVolcanoPossible($x, $y)
                    ];
                    if (isset($available[$key])) {
                        $currentAvailable = $available[$key];
                    }
        
                    $currentAvailable["restrictions"][$this->getOppositeDirection($unconnected)] = $node->getField()->getConnectionType($unconnected);

                    //if neighbour exists add its connections to restrictions
                    if ($neighbouringField !== null){
                        $neighboringNodes = $graph->getAllNodes($x, $y);
                        foreach ($neighboringNodes as $neighboringNode) {
                            foreach ($neighboringNode->getEdges() as $direction => $_) {
                                $currentAvailable["restrictions"][$direction] = $neighboringNode->getField()->getConnectionType($direction);
                                if ($neighbouringField->getRouteType() != RRIField::TYPE_METEOR) {
                                    $currentAvailable["onlyRewritable"] = true;
                                }
                            }
                        }
                    }

                    $available[$key] = $currentAvailable;
                }
            }
        }
        if (in_array($this->expansion, [RRIField::TYPE_RIVER, RRIField::TYPE_LAKE])){
            for ($x=0; $x < 7; $x++) { 
                for ($y=0; $y < 7; $y++) {
                    if (!array_key_exists($x.$y, $available)
                        && !array_key_exists(intval($x.$y), $available) 
                        && !$this->isFieldExists($x, $y)){
                        $newAvailableField = [
                            "x" => $x,
                            "y" => $y,
                            "restrictions" => []
                        ];
                        $available[$x.$y] = $newAvailableField;
                    }
                }
            }
        }

        if ($this->expansion == RRIField::TYPE_LAVA){
            for ($x=0; $x < 7; $x++) { 
                for ($y=0; $y < 7; $y++) {
                    if (!array_key_exists($x.$y, $available)
                        && !array_key_exists(intval($x.$y), $available)){

                        $newAvailableField = [
                            "x" => $x,
                            "y" => $y,
                            "restrictions" => [],
                            "isVolcanoPossible" => $this->isVolcanoPossible($x, $y)
                        ];
                        
                        $available[$x.$y] = $newAvailableField;
                    }
                }
            }
        }

        return $available;
    }

    private function isVolcanoPossible($x, $y){
        if ($this->expansion !== RRIField::TYPE_LAVA){
            return false;
        }

        $neighbourCoords = [
            [ "x" => $x - 1, "y" => $y],
            [ "x" => $x + 1, "y" => $y],
            [ "x" => $x, "y" => $y - 1],
            [ "x" => $x, "y" => $y + 1],
        ];

        foreach ($neighbourCoords as $coord) {
            $field = $this->getField($coord["x"], $coord["y"]);
            if ($field !== null && $field->getRouteType() === RRIField::TYPE_LAVA){
                return false;
            }
        }

        return true;
    }

    private function getOppositeDirection($dir)
    {
        switch ($dir) {
            case 'N':
                return "S";
            case 'S':
                return "N";
            case 'E':
                return "W";
            case 'W':
                return "E";
            default:
                return null;
        }
    }

    public function getScore() {
        $score = array_merge(
            $this->calculateCentralFieldsScore(),
            $this->calculateErrorsScore(),
            $this->calculateExitsScore(),
            $this->calculateLongestPathsScore(),
            $this->calcluateScoreForExpansion()
        );

        $total = 0;
        foreach ($score as $scorePart) {
            $total += $scorePart["total"];
        }

        $score["total"] = $total;

        return $score;
    }

    private function calculateCentralFieldsScore(){
        $occupiedFields = [];
        for ($x=2; $x < 5; $x++) { 
            for ($y=2; $y < 5; $y++) { 
                $key = $this->fieldKey($x, $y);
                
                if (isset($this->fields[$key])){
                    $occupiedFields[] = $this->fields[$key];
                }
            }
        }

        return [
            "centralFields" => [
                "total" => sizeof($occupiedFields),
                "fields" => $this->serializeFieldCoordinates($occupiedFields)
            ]
        ];
    }

    private function calculateErrorsScore(){

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);
        $freeEndNodes = $graph->getFreeEndNodes();

        $allErrorDirections = [];
        foreach ($freeEndNodes as $node) {
            $nodeErrors = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());

            foreach ($nodeErrors as $error) {
                if ($node->getField()->isSideConnectionTypeError($error)){
                    $destinationFieldKey = $node->getField()->getNeighbour($error)["key"];
                    if (!array_key_exists($destinationFieldKey, $this->fields) 
                        || $this->fields[$destinationFieldKey]->getRouteType() != RRIField::TYPE_METEOR){
                        $allErrorDirections[] = [
                            "field" => $node->getField()->serializeFieldCoordinates(),
                            "direction" => $error
                        ];
                    }
                }
            }
        }

        return [
            "errors" => [
                "total" => -1 * sizeof($allErrorDirections),
                "allErrorDirections" => $allErrorDirections
            ]
        ];
    }

    private function calculateExitsScore() {
        $exitGroups = [];

        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false ]);
        $freeEndNodes = $graph->getFreeEndNodes();

        $currentExitGroup = null;
        $visited = [];
        $traverse = function($node) use (&$currentExitGroup, &$traverse, &$visited) {
            if (array_search($node, $visited) !== false){
                return;
            }

            $visited[] = $node;
            
            $currentExitGroup["fields"][] = $node->getField()->serializeFieldCoordinates();

            if ($this->isConnectedToStartingField($node->getField())){
                $currentExitGroup["exits"][] = $node->getField()->serializeFieldCoordinates();
            }

            foreach ($node->getEdges() as $edge) {
                $traverse($edge);
            }
        };

        while (sizeof($freeEndNodes) > 0) {
            $node = array_pop($freeEndNodes);

            if ($this->isConnectedToStartingField($node->getField())){
                $currentExitGroup = [
                    "fields" => [],
                    "exits" => []
                ];

                $traverse($node);

                $exitGroups[] = $currentExitGroup;
            }
            
        }
        
        $total = 0;
        foreach ($exitGroups as &$group) {
            $score = self::EXIT_SCORE_MAP[sizeof($group["exits"])];
            $total += $score;
            $group["score"] = $score;
        }

        return [
            "exits" => [
                "total" => $total,
                "exitGroups" => $exitGroups
            ]
        ];
    }

    private function findLongestPath($currentNode, $direction, $usedDirections = []){
        $currentField = $currentNode->getField();
        $allPossibleDirections = $currentField->getDirectionsFrom($direction);

        $fieldKey = $this->fieldKey($currentField->getX(), $currentField->getY());

        if (!array_key_exists($fieldKey, $usedDirections)){
            $usedDirections[$fieldKey] = [];
        }
        $usedDirections[$fieldKey][] = $direction;

        $nextDirections = array_diff($allPossibleDirections, $usedDirections[$fieldKey]);
        
        if (sizeof($nextDirections) == 0){
            return [$currentField];
        }

        $continuingPath = [];
        
        foreach ($nextDirections as $next) {
            if ($currentNode->isEdgeExists($next)){
                $usedDirections[$fieldKey][] = $next;
                $possiblePath = $this->findLongestPath($currentNode->getEdge($next), $this->getOppositeDirection($next), $usedDirections);

                if (sizeof($possiblePath) > sizeof($continuingPath)){
                    $continuingPath = $possiblePath;    
                }
            }
        }

        $continuingPath[] = $currentField;

        return $continuingPath;
    }

    private function calculateLongestPathsScore(){
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);

        $freeEndNodes = $graph->getFreeEndNodes();

        $longestRoad = [];
        $longestRailroad = [];

        $resolver = function($type, $path) use (&$longestRailroad, &$longestRoad) {
            if ($type === "H" && sizeof($path) > sizeof($longestRoad)){
                $longestRoad = $path;
            }
            
            if ($type === "R" && sizeof($path) > sizeof($longestRailroad)){
                $longestRailroad = $path;
            }
        };

        foreach ($freeEndNodes as $current) {
            foreach ($current->getUnconnectedDirections() as $direction) {
                $type = $current->getField()->getConnectionType($direction);
                $resolver($type, $this->findLongestPath($current, $direction));
            }
        }

        foreach ($graph->getNodesWithStation() as $current) {
            $includeOffBoard = true;
            foreach ($current->getField()->getFieldDirections($includeOffBoard) as $direction) {
                $type = $current->getField()->getConnectionType($direction["direction"]);

                $path = [$current->getField()];
                if (array_key_exists($direction["direction"], $current->getEdges())){
                    $usedDirections = [
                        $this->fieldKey($current->getField()->getX(), $current->getField()->getY()) => [$direction["direction"]]
                    ]; 
                    $path = array_merge($path, $this->findLongestPath($current->getEdges()[$direction["direction"]], $this->getOppositeDirection($direction["direction"]), $usedDirections));
                }

                $resolver($type, $path);
            }
        }
        
        return [
            "longestRailroad" => [
                "fields" => $this->serializeFieldCoordinates($longestRailroad),
                "total" => sizeof($longestRailroad)
            ],
            "longestHighway" => [
                "fields" => $this->serializeFieldCoordinates($longestRoad),
                "total" => sizeof($longestRoad)
            ]
        ];
    }

    private function calcluateScoreForExpansion(){
        if ($this->expansion < 2){
            return [];
        }

        if ($this->expansion == railroadink::EXPANSION_RIVER_OPTION_VALUE){
            return $this->calculateRiverScore();
        }
        if ($this->expansion == railroadink::EXPANSION_LAKE_OPTION_VALUE){
            return $this->calculateLakeScore();
        }
        if ($this->expansion == railroadink::EXPANSION_METEOR_OPTION_VALUE) {
            return $this->calculateMeteorScore();
        }
        if ($this->expansion == railroadink::EXPANSION_LAVA_OPTION_VALUE) {
            return $this->calculateLavaScore();
        }
    }

    private function calculateLavaScore() {
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);

        $lavaNodes = $graph->getNodesWithSpecificRouteType(railroadink::EXPANSION_LAVA_OPTION_VALUE);

        $largestLavaFields = [];
        $largestLavaScore = null;

        $closedScore = 0;

        while (sizeof($lavaNodes) > 0){
            $current = array_values($lavaNodes)[0];
            $isClosed = true;

            $currentLavaFields = [];
            $nodesToVisit = [$current];

            while ($node = array_pop($nodesToVisit)){
                $index = array_search($node, $lavaNodes); 

                if ($index !== false) {
                    unset($lavaNodes[$index]);
                    $currentLavaFields[] = $node->getField();

                    foreach ($node->getEdges() as $next) {
                        $nodesToVisit[] = $next;
                    }
                }

                $insideUnconnected = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());
                if (sizeof($insideUnconnected) > 0){
                    $isClosed = false;
                }
            }

            $score = sizeof($currentLavaFields);

            if ($isClosed){
                $closedScore += 5;
            }

            if ($largestLavaScore === null || $score > $largestLavaScore){
                $largestLavaScore = $score;
                $largestLavaFields = $currentLavaFields;
            }
            
        }

        return [
            "special" => [
                "fields" => $this->serializeFieldCoordinates($largestLavaFields),
                "total" => $largestLavaScore + $closedScore,
                "type" => "lava"
            ]
        ];
    }

    private function calculateMeteorScore(){
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);
        $freeEndNodes = $graph->getFreeEndNodes();

        $fields = [];
        foreach ($freeEndNodes as $node) {
            $nodeErrors = array_diff($node->getUnconnectedDirections(), $node->getOutOfBoardConnections());

            foreach ($nodeErrors as $error) {
                $destinationFieldKey = $node->getField()->getNeighbour($error)["key"];
                if (array_key_exists($destinationFieldKey, $this->fields) 
                    && $this->fields[$destinationFieldKey]->getRouteType() == RRIField::TYPE_METEOR){
                    $fields[] = $node->getField();
                }
            }
        }

        return [
            "special" => [
                "fields" => $this->serializeFieldCoordinates($fields),
                "total" => sizeof($fields) * 2,
                "type" => "meteor"
            ]
        ];
    }
    private function calculateRiverScore(){
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);

        $riverNodes = $graph->getNodesWithSpecificRouteType(railroadink::EXPANSION_RIVER_OPTION_VALUE);

        $longestRiverFields = [];
        $longestRiverScore = 0;

        while (sizeof($riverNodes) > 0){
            $current = array_values($riverNodes)[0];

            $currentRiverFields = [];
            $nodesToVisit = [$current];

            $numberOfOutOfBoundConnections = 0;

            while ($node = array_pop($nodesToVisit)){
                $index = array_search($node, $riverNodes); 

                if ($index !== false) {
                    unset($riverNodes[$index]);
                    $currentRiverFields[] = $node->getField();

                    foreach ($node->getEdges() as $direction => $next) {
                        if ($node->getField()->getConnectionType($direction) === "W"){
                            $nodesToVisit[] = $next;
                        }
                    }
                    
                    $numberOfOutOfBoundConnections += sizeof($node->getOutOfBoardConnections());
                }

            }

            $score = sizeof($currentRiverFields);

            if ($numberOfOutOfBoundConnections == 2){
                $score += 3;
            }

            if ($longestRiverScore === null || $score > $longestRiverScore){
                $longestRiverScore = $score;
                $longestRiverFields = $currentRiverFields;
            }
            
        }

        return [
            "special" => [
                "fields" => $this->serializeFieldCoordinates($longestRiverFields),
                "total" => $longestRiverScore,
                "type" => "river"
            ]
        ];
    }
    
    private function calculateLakeScore(){
        $graph = new RRIGraph([ "fields" => $this->fields, "addEntryNodes" => false]);

        $lakeNodes = $graph->getNodesWithSpecificRouteType(railroadink::EXPANSION_LAKE_OPTION_VALUE);

        $smallestLakeFields = [];
        $smallestLakeScore = null;

        while (sizeof($lakeNodes) > 0){
            $current = array_values($lakeNodes)[0];

            $currentLakeFields = [];
            $nodesToVisit = [$current];

            while ($node = array_pop($nodesToVisit)){
                $index = array_search($node, $lakeNodes); 

                if ($index !== false) {
                    unset($lakeNodes[$index]);
                    $currentLakeFields[] = $node->getField();

                    foreach ($node->getEdges() as $direction => $next) {
                        if ($node->getField()->getConnectionType($direction) === "L"){
                            $nodesToVisit[] = $next;
                        }
                    }
                }
            }

            $score = sizeof($currentLakeFields);

            if ($smallestLakeScore === null || $score < $smallestLakeScore){
                $smallestLakeScore = $score;
                $smallestLakeFields = $currentLakeFields;
            }
            
        }

        return [
            "special" => [
                "fields" => $this->serializeFieldCoordinates($smallestLakeFields),
                "total" => $smallestLakeScore ?? 0,
                "type" => "lake"
            ]
        ];
    }

    private function isConnectedToStartingField($field) {
        $isStartingField = array_search($field->getX(), [0, 6]) !== false && array_search($field->getY(), [1, 3, 5]) !== false  
            || array_search($field->getY(), [0, 6]) !== false && array_search($field->getX(), [1, 3, 5]) !== false;

        if ($isStartingField){
            if ($field->getX() == 0){
                $type = $field->getY() == 3 ? "H" : "R";
                return $field->checkIfConnected("E", $type);
            }
            if ($field->getX() == 6){
                $type = $field->getY() == 3 ? "H" : "R";
                return $field->checkIfConnected("W", $type);
            }
            if ($field->getY() == 0){
                $type = $field->getX() == 3 ? "R" : "H";
                return $field->checkIfConnected("S", $type);
            }
            if ($field->getY() == 6){
                $type = $field->getX() == 3 ? "R" : "H";
                return $field->checkIfConnected("N", $type);
            }
        }

        return false;
    }

    public function findFieldSurroundedBy($sideType, $targetCount){
        for ($x=0; $x < 7; $x++) { 
            for ($y=0; $y < 7; $y++) { 
                if (!$this->isFieldExists($x, $y)){
                    $count = 0;

                    $neighbourChecks = [
                        [ "key" => $this->fieldKey($x - 1, $y), "direction" => "E"],
                        [ "key" => $this->fieldKey($x + 1, $y), "direction" => "W"],
                        [ "key" => $this->fieldKey($x, $y - 1), "direction" => "S"],
                        [ "key" => $this->fieldKey($x, $y + 1), "direction" => "N"],
                    ];

                    foreach ($neighbourChecks as $neighbourCheck) {
                        $neighbourKey = $neighbourCheck["key"];
                        if (array_key_exists($neighbourKey, $this->fields)){
                            $neighbourSide = $this->fields[$neighbourKey]->getConnectionType($neighbourCheck["direction"]);
                            if ($neighbourSide == $sideType){
                                $count++;
                            }
                        }
                    }

                    if ($count >= $targetCount){
                        return [
                            "x" => $x,
                            "y" => $y
                        ];
                    }

                }
            }
        }

        return null;
    }

    private function findLastMeteor() {
        foreach ($this->fields as $field) {
            if ($field->isLastMeteor()){
                return [
                    "x" => $field->getX(),
                    "y" => $field->getY()
                ];
            }
        }

        return [
            "x" => 3,
            "y" => 3
        ];
    }

    public function calculateMeteorTargets($distanceDie, $directionDie){
        if ($directionDie == 7){
            $allDirections = array_merge(
                $this->calculateMeteorTargets($distanceDie, 3),
                $this->calculateMeteorTargets($distanceDie, 4),
                $this->calculateMeteorTargets($distanceDie, 5),
                $this->calculateMeteorTargets($distanceDie, 6)
            );
            
            return array_values(array_unique($allDirections, SORT_REGULAR));
        }

        $target = $this->findLastMeteor();
        $distance = $distanceDie + 1;

        $multiplier = 1;
        if ($directionDie == 4 || $directionDie == 5){
            $multiplier = -1;
        }

        $axis = "x";
        if ($directionDie > 4){
            $axis = "y";
        }

        $target[$axis] += $multiplier * $distance;

        if ($target[$axis] < 0){
            $target[$axis] *= -1;
        }

        if ($target[$axis] > 6){
            $target[$axis] = 6 - ($target[$axis] - 6); 
        }

        //check if meteor is already on target
        if ($this->isFieldExists($target["x"], $target["y"])){
            $targetField = $this->fields[$this->fieldKey($target["x"], $target["y"])]; 
            if ($targetField->getRouteType() == RRIField::TYPE_METEOR){
                if ($distanceDie > 9){
                    return [];
                }
                return $this->calculateMeteorTargets($distanceDie + 1, $directionDie);
            }
        }

        return [$target];
    }

    public function getField($x, $y){
        if ($this->isFieldExists($x, $y)){
            return $this->fields[$this->fieldKey($x, $y)];
        }

        return null;
    }
}
