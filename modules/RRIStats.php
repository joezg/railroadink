<?php

class RRIStats
{
    const TOTAL_POINTS = "total_points";
    const POINTS_FROM_EXITS = "points_from_exits";
    const POINTS_FROM_LONGEST_HIGHWAY = "points_from_longest_highway";
    const POINTS_FROM_LONGEST_RAILROAD = "points_from_longest_railroad";
    const POINTS_FROM_CENTER = "points_from_center";
    const POINTS_FROM_EXPANSION = "points_from_expansion";
    const LOST_POINTS_ERRORS = "lost_points_errors";
    const USED_SPECIAL_ROUTES = "used_special_routes";
    const SPECIAL_ROUTE_LAST_ROUND = "special_route_last_round";
    const TOTAL_EXIT_GROUPS = "total_exit_groups";
    const TWO_EXITS_CONNECTED = "two_exits_connected";
    const THREE_EXITS_CONNECTED = "three_exits_connected";
    const FOUR_EXITS_CONNECTED = "four_exits_connected";
    const FIVE_EXITS_CONNECTED = "five_exits_connected";
    const SIX_EXITS_CONNECTED = "six_exits_connected";
    const SEVEN_EXITS_CONNECTED = "seven_exits_connected";
    const EIGHT_EXITS_CONNECTED = "eight_exits_connected";
    const NINE_EXITS_CONNECTED = "nine_exits_connected";
    const TEN_EXITS_CONNECTED = "ten_exits_connected";
    const ELEVEN_EXITS_CONNECTED = "eleven_exits_connected";
    const TWELVE_EXITS_CONNECTED = "twelve_exits_connected";
    const PLAYED_RIVER_EXPANSION = "played_river_expansion";
    const PLAYED_LAKE_EXPANSION = "played_lake_expansion";
    const PLAYED_LAVA_EXPANSION = "played_lava_expansion";
    const PLAYED_METEOR_EXPANSION = "played_meteor_expansion";
}